<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funcionarios extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
		$this->load->model('Funcionarios_model');
	}

	public function index()
	{
		$dadosView['dados'] = $this->Funcionarios_model->listar();

		$dadosView['meio'] = 'funcionarios/listar';
		$this->load->view('tema/layout',$dadosView);	
	}

	public function adicionar()
	{
	    $this->form_validation->set_rules('funcionario_cpf', 'CPF', 'trim|required|is_unique[funcionarios.funcionario_cpf]');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {
      
        	$dados = array(        		 				 
				  'funcionario_cpf'                 => $this->input->post('funcionario_cpf'),
				  'funcionario_nome'                => $this->input->post('funcionario_nome'),
				  'funcionario_endereco'            => $this->input->post('funcionario_endereco'),
				  'funcionario_numero' 				=> $this->input->post('funcionario_numero'),
				  'funcionario_complemento' 		=> $this->input->post('funcionario_complemento'),
				  'funcionario_bairro' 				=> $this->input->post('funcionario_bairro'),
				  
				  'funcionario_estado' 				=> $this->input->post('funcionario_estado'),
				  'funcionario_cidade' 				=> $this->input->post('funcionario_cidade'),
				  'funcionario_cep' 				=> $this->input->post('funcionario_cep'),
				  'funcionario_telefone' 			=> $this->input->post('funcionario_telefone'),
				  'funcionario_celular' 			=> $this->input->post('funcionario_celular'),
				  'funcionario_data_cadastro' 		=> date('Y-m-d'),
				  'funcionario_email' 				=> $this->input->post('funcionario_email'),		  
				  'funcionario_matricula' 		    => $this->input->post('funcionario_matricula'),
				  'funcionario_cargo'               => $this->input->post('funcionario_cargo'),
        	);
                	
        	$resultado = $this->Funcionarios_model->adicionar($dados);

        	if($resultado){
        		$this->session->set_flashdata('success','Registro adicionado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        }

		$dadosView['estados'] = $this->Funcionarios_model->pegarEstados();
		$dadosView['cargos']   = $this->Funcionarios_model->pegarCargos();

		$dadosView['meio'] = 'funcionarios/adicionar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function editar()
	{
		$this->form_validation->set_rules('funcionario_cpf', 'CPF', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {


        	$dados = array(        		 				 
				  'funcionario_cpf'                 => $this->input->post('funcionario_cpf'),
				  'funcionario_nome'                => $this->input->post('funcionario_nome'),
				  'funcionario_endereco'            => $this->input->post('funcionario_endereco'),
				  'funcionario_numero' 				=> $this->input->post('funcionario_numero'),
				  'funcionario_complemento' 		=> $this->input->post('funcionario_complemento'),
				  'funcionario_bairro' 				=> $this->input->post('funcionario_bairro'),
				  
				  'funcionario_estado' 				=> $this->input->post('funcionario_estado'),
				  'funcionario_cidade' 				=> $this->input->post('funcionario_cidade'),
				  'funcionario_cep' 				=> $this->input->post('funcionario_cep'),
				  'funcionario_telefone' 			=> $this->input->post('funcionario_telefone'),
				  'funcionario_celular' 			=> $this->input->post('funcionario_celular'),
				  'funcionario_email' 				=> $this->input->post('funcionario_email'),			
				  'funcionario_matricula'           => $this->input->post('funcionario_matricula'),	  	
				  'funcionario_cargo'               => $this->input->post('funcionario_cargo'),
        	);

        	if($this->input->post('funcionario_matricula')){
        		
        		$dados = array_merge($dados,['funcionario_matricula' => sha1(md5(strtolower($this->input->post('funcionario_matricula'))))]);
        	}
     
        	$resultado = $this->Funcionarios_model->editar($dados,$this->input->post('id'));

        	if($resultado){
        		$this->session->set_flashdata('success','Registro editado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao editado o registro!');
        	}
        }

        $this->load->model('Sistema_model');

        $dadosView['dados']   = $this->Funcionarios_model->listarId($this->uri->segment(3));
		$dadosView['cargos']   = $this->Funcionarios_model->pegarCargos();
		$dadosView['estados'] = $this->Funcionarios_model->pegarEstados();
		$dadosView['cidades'] = $this->Sistema_model->selecionarCidades($dadosView['dados'][0]->funcionario_estado);
		$dadosView['meio']    = 'funcionarios/editar';

		$this->load->view('tema/layout',$dadosView);

	}

	public function visualizar()
	{
		$this->load->model('Sistema_model');
		
		$dadosView['dados']   = $this->Funcionarios_model->listarId($this->uri->segment(3));
		$dadosView['nivel']   = $this->Funcionarios_model->pegarCargos();
		$dadosView['estados'] = $this->Funcionarios_model->pegarEstados();
		$dadosView['cidades'] = $this->Sistema_model->selecionarCidades($dadosView['dados'][0]->funcionario_estado);

		$dadosView['meio']  = 'funcionarios/visualizar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function excluir()
	{
		$id = $this->uri->segment(3);

		$dados  = array(
						'funcionario_visivel' => 0					
					  );
		$resultado = $this->Funcionarios_model->excluir($dados,$id);

		if($resultado){
			$this->session->set_flashdata('success','registro excluidos com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir o registro!');
		}

		redirect('Funcionarios','refresh');
	}

	public function autoCompleteFuncionarios()
	{
		$termo = strtolower($this->input->get('term'));
        $this->Funcionarios_model->autoCompleteFuncionarios($termo);
	}
}
