<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios extends CI_Controller {

	public function __construct()
	{   
		parent::__construct();
		$this->load->model('Relatorios_model');
		$this->load->model('Sistema_model');

		$this->load->library('Ciqrcode');
		$this->load->library('Zend');
	}

	public function clientes()
	{

		if($this->input->post() || $this->uri->segment(3) <> null){

			if($this->uri->segment(3) == 'todos'){
				$dadosView['dados']    = $this->Relatorios_model->clientes();
			}

			if($this->uri->segment(3) == 'mes'){
				$mes = true;
				$dadosView['dados']    = $this->Relatorios_model->clientes($mes);
			}
			
			$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
			$dadosView['meio']     = 'relatorios/clientes/clientes_simples';

			$html = $this->load->view('relatorios/impressao',$dadosView, true);	
			
			gerarPDF($html);
		}else{
			$dadosView['totalcliente']  = $this->Relatorios_model->clientesTotal();	
			$dadosView['totalAniversariante'] = $this->Relatorios_model->clientesTotalAnive();

			$dadosView['meio']     = 'relatorios/clientes/clientes';
			$this->load->view('tema/layout',$dadosView);
		}
	}

	public function produtos()
	{

		if($this->input->post() || $this->uri->segment(3) <> null){

			if($this->uri->segment(3) == 'todos'){
				$dadosView['dados']    = $this->Relatorios_model->produtos();
			}

			if($this->uri->segment(3) == 'minimo'){				
				$dadosView['dados']    = $this->Relatorios_model->estoqueMinimo();
			}

			if($this->uri->segment(3) == 'valorestoque'){				
				$dadosView['dados']    = $this->Relatorios_model->valorEstoque();
			}
			
			$dadosView['emitente'] = $this->Sistema_model->pegarEmitente();
			$dadosView['meio']     = 'relatorios/produtos/produtos_simples';

			$html = $this->load->view('relatorios/impressao',$dadosView, true);	
			
			gerarPDF($html);
		}else{
			$dadosView['totalProduto']        = $this->Relatorios_model->totalProduto();	
			$dadosView['totalEstoqueMinimo']  = $this->Relatorios_model->totalEstoqueMinimo();
			$dadosView['totalValorEstoque']   = $this->Relatorios_model->totalValorEstoque();


			$dadosView['grupo'] = $this->Relatorios_model->pegarGrupoProduto();
			
			$dadosView['meio']     = 'relatorios/produtos/produtos';
			$this->load->view('tema/layout',$dadosView);
		}
	}

	public function produtosEtiquetaCustom()
	{	

		$tipo  = $this->input->post('tipo_etiqueta');
		$grupo = $this->input->post('produto_categoria_id');

		
		$dadosView['tipo']       =  $tipo;
		$etiquetas  =  $this->Relatorios_model->pegarEtiquetaProduto($grupo);

		if ($tipo == '01') { // Cód Barra
			foreach ($etiquetas as $e) {			
				$e->url = base_url('relatorios/Barcode/'.$e->produto_codigo_barra);
				
			}	
		}

		if ($tipo == '02') { // Qr-Code			
			foreach ($etiquetas as $e) {
				$e->url = base_url('relatorios/QRcode/'.$e->produto_codigo_barra);
				
			}	
		}

		$dadosView['etiquetas'] =  $etiquetas;
		$dadosView['emitente']  =  $this->Sistema_model->pegarEmitente();
		//$dadosView['meio']    = 'relatorios/produtos/produtos_etiquetas';

		$this->load->view('relatorios/produtos/produtos_etiquetas',$dadosView);
				
		// gerarPDF($html);

	}

	public function QRcode($value)
	{	
		QRcode::png(
			$value,
			$outfile = false,
			$level = QR_ECLEVEL_H,
			$size  = 6,
			$margin = 1
		);
	}

	public function Barcode($value)
	{
		$this->zend->load('Zend/Barcode');
		Zend_Barcode::render('code128','image', array('text' =>  $value));
	}




	
}
