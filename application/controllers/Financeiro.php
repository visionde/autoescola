<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financeiro extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
		$this->load->model('Financeiro_model');
	}

	public function index($page = 1, $situacao = NULL, $periodo = NULL, $cliente = NULL, $cliente_fornecedor = NULL)
	{
        $where = '';
        $situacao = isset($_GET['situacao']) ? $_GET['situacao'] : $situacao;
        $periodo = isset($_GET['periodo']) ? $_GET['periodo'] : $periodo;
        $cliente = isset($_GET['clie_forn_id']) ? $_GET['clie_forn_id'] : $cliente;
        $cliente_fornecedor =  isset($_GET['cliente']) ? $_GET['cliente'] : $cliente_fornecedor;

	     // busca todos os lançamentos
	    if($periodo == 'todos'){

	        if($situacao == 'previsto'){
	            $where = 'data_vencimento > "'.date('Y-m-d').'" AND financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1'; 
	        }
	        else{
	            if($situacao == 'atrasado'){
	                $where = 'data_vencimento < "'.date('Y-m-d').'" AND financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1'; 
	            }
	            else{
	                if($situacao == 'realizado'){
	                    $where = 'financeiro_baixado = "1" AND financeiro.`financeiro_visivel` = 1';
	                }
	                if($situacao == 'pendente'){
	                    $where = 'financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1';
	                }
	            }
	        }
	    }
	    else {
	        // busca lançamentos do dia 

	        if($periodo == null || $periodo == 'dia'){
	            $where = 'data_vencimento = "'.date('Y-m-d'.'"');
	             if(! isset($situacao) || $situacao == 'todos'){  
	                    $where = 'data_vencimento = "'.date('Y-m-d'.'"').' AND financeiro_baixado in ("0","1") AND financeiro.`financeiro_visivel` = 1'; 
	                }
	                else{
	                    if($situacao == 'previsto'){
	                         $where = 'data_vencimento >= "'.date('Y-m-d'.'"').' AND financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1';
	                    }
	                    else{
	                        if($situacao == 'atrasado'){
	                            $where = 'data_vencimento <= "'.date('Y-m-d'.'"').' AND financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1'; 
	                        }
	                        else{
	                            if($situacao == 'realizado'){
	                                $where = 'data_vencimento = "'.date('Y-m-d'.'"').' AND financeiro_baixado = "1" AND financeiro.`financeiro_visivel` = 1';
	                            }
	                            else{
	                                $where = 'data_vencimento = "'.date('Y-m-d'.'"').' AND financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1';
	                            }
	                        }
	                    }
	                } 

	            } // fim lançamentos dia
	        else {
	            // busca lançamentos da semana
	            if($periodo == 'semana') {
	                $semana = $this->getThisWeek();
	                if(! isset($situacao) || $situacao == 'todos'){ 
	                    $where = 'data_vencimento BETWEEN "'.$semana[0].'" AND "'.$semana[1].'" AND financeiro.`financeiro_visivel` = 1'; 
	                
				 	}
	                else{
	                    if($situacao == 'previsto'){
	                        $where = 'data_vencimento BETWEEN "'.date('Y-m-d').'" AND "'.$semana[1].'" AND financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1'; 
	                    }
	                    else{
	                        if($situacao == 'atrasado'){
	                            $where = 'data_vencimento BETWEEN "'.$semana[0].'" AND "'.date('Y-m-d').'" AND financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1'; 
	                        }
	                        else{
	                            if($situacao == 'realizado'){
	                                $where = 'data_vencimento BETWEEN "'.$semana[0].'" AND "'.$semana[1].'" AND financeiro_baixado = "1" AND financeiro.`financeiro_visivel` = 1';
	                            }
	                            else{
	                                $where = 'data_vencimento BETWEEN "'.$semana[0].'" AND "'.$semana[1].'" AND financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1';
	                            }
	                        }
	                    }
	                }                

	            } // fim lançamentos Semana
	            else{
	                // busca lançamento do mês
	                if($periodo == 'mes'){          

	                    $mes = $this->getThisMonth();                       

	                    if(! isset($situacao) || $situacao == 'todos'){  

	                        $where = 'data_vencimento BETWEEN "'.$mes[0].'" AND "'.$mes[1].'" AND financeiro.`financeiro_visivel` = 1'; 
	                    }
	                    else{
	                        if($situacao == 'previsto'){
	                            $where = 'data_vencimento BETWEEN "'.date('Y-m-d').'" AND "'.$mes[1].'" AND financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1'; 
	                        }
	                        else{
	                            if($situacao == 'atrasado'){
	                                $where = 'data_vencimento BETWEEN "'.$mes[0].'" AND "'.date('Y-m-d').'" AND financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1';
	                            }
	                            else{
	                                if($situacao == 'realizado'){
	                                    $where = 'data_vencimento BETWEEN "'.$mes[0].'" AND "'.$mes[1].'" AND financeiro_baixado = "1" AND financeiro.`financeiro_visivel` = 1';  
	                                }
	                                else{
	                                    $where = 'data_vencimento BETWEEN "'.$mes[0].'" AND "'.$mes[1].'" AND financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1';
	                                }                            

	                            }
	                        }
	                    }
	                }
	                // busca lançamentos do ano

	                else{
	                    $ano = $this->getThisYear();     

	                    if(! isset($situacao) || $situacao == 'todos'){  

	                        $where = 'data_vencimento BETWEEN "'.$ano[0].'" AND "'.$ano[1].'" AND financeiro.`financeiro_visivel` = 1';
	                    }
	                    else{
	                        if($situacao == 'previsto'){
	                            $where = 'data_vencimento BETWEEN "'.date('Y-m-d').'" AND "'.$ano[1].'" AND financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1'; 
	                        }
	                        else{
	                            if($situacao == 'atrasado'){
	                                $where = 'data_vencimento BETWEEN "'.$ano[0].'" AND "'.date('Y-m-d').'" AND financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1'; 
	                            }
	                            else{
	                                if($situacao == 'realizado'){
	                                    $where = 'data_vencimento BETWEEN "'.$ano[0].'" AND "'.$ano[1].'" AND financeiro_baixado = "1" AND financeiro.`financeiro_visivel` = 1';     

	                                }
	                                else{
	                                    $where = 'data_vencimento BETWEEN "'.$ano[0].'" AND "'.$ano[1].'" AND financeiro_baixado = "0" AND financeiro.`financeiro_visivel` = 1';
	                                }                        
	                            }
	                        }
	                    }  
	                }
	            }
	        }   
	     }


	     // if($cliente != ''){
	     //    if($where != ''){
	     //         $where .= ' AND financeiro_forn_clie_id ="'.$cliente.'"';
	     //    }else{
	     //         $where .= 'financeiro_forn_clie_id ="'.$cliente.'"';
	     //    }           
	     // }

	     if($cliente_fornecedor != ''){
	        if($where != ''){
	             $where .= ' AND financeiro_forn_clie  LIKE'. "'%".$cliente_fornecedor."%'";
	        }else{
	             $where = 'financeiro_forn_clie LIKE '. "'%".$cliente_fornecedor."%'";
	        }           

	     }
	     $pesquisa = $where;
         $itens_per_page = 15;
         $data['dados']  = $this->Financeiro_model->listar($page, $itens_per_page, $pesquisa);
         $data['meio']   = 'financeiro/listar';
         $total = $this->Financeiro_model->contador($page, $pesquisa);
         if ($total){
             $data['total_financeiro']   = $total[0]->total;
         }else{
             $data['total_financeiro']   = null;
         }
         $data['page']   = $page;
         $data['situacao']   = $situacao;
         $data['periodo']   = $periodo;
         $data['cliente']   = $cliente;
         $data['cliente_fornecedor']   = $cliente_fornecedor;
		 $data['itens_per_page'] = $itens_per_page;
		 $data['cartao'] = $this->Financeiro_model->getAllTiposCartao();
         $data['categoriaFin'] = $this->Financeiro_model->getAllTiposCategorias();   
		 $this->load->view('tema/layout',$data);
         // var_dump($data);exit();
	}

	public 	function adicionarReceita() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        $urlAtual = $this->input->post('urlAtual');

       // echo "<pre>";  var_dump($this->input->post('recebido')); exit();

        $this->form_validation->set_rules('clie_receita', '', 'trim|required');

        if ($this->form_validation->run('receita') == false) {
          $this->session->set_flashdata('erro',validation_errors());
          redirect($urlAtual);
        } else {

            $vencimento = $this->input->post('vencimento');
            $recebimento = $this->input->post('recebimento');

            if (empty($recebimento)) {
                $recebimento = '00/00/0000';
            }else{
                $recebimento = $recebimento = explode('/',  $this->input->post('recebimento'));
                $recebimento = $recebimento = $recebimento[2].'-'.$recebimento[1].'-'.$recebimento[0];
            }

            if($vencimento == null){
                $vencimento = date('d/m/Y');
            }            

            try {  
                $vencimento = explode('/', $vencimento);
                $vencimento = $vencimento[2].'-'.$vencimento[1].'-'.$vencimento[0]; 
            } catch (Exception $e) {
               $vencimento = date('Y/m/d');
            }

            if ($this->input->post('recebido') == NULL) { 
            	$recebido = 0 ;
            }else { 
            	$recebido = $this->input->post('recebido');
            }            

            /* NOVA ROTINA -- INICIO -- */
            $cont = $this->input->post('qtdFormaPagamento');
            $valorTotal = 0;

            for ($i=1; $i <= $cont; $i++) { 

                if($i != 1){
                    $dados[$i]['valor']      = floatval($this->input->post('valor'.$i));
                    $dados[$i]['vencimento'] = $this->input->post('vencimento'.$i);
                    $dados[$i]['formaPgto']  = $this->input->post('formaPgto'.$i);
                }else{
                    $dados[$i]['valor']      = floatval($this->input->post('valor'));
                    $dados[$i]['vencimento'] = $this->input->post('vencimento');
                    $dados[$i]['formaPgto']  = $this->input->post('formaPgto');
                }

                $valorTotal += $dados[$i]['valor'];
            }

            $contData = 0;


            foreach ($dados as $valores) {                        

                try {
                    
                    $vencimento = explode('/', $valores['vencimento']);
                    $vencimento = $vencimento[2].'-'.$vencimento[1].'-'.$vencimento[0];
                    
                   
                } catch (Exception $e) {
                   $vencimento = date('Y/m/d'); 
                  
                }

                $clie_forn = explode('|', set_value('clie_receita'));

                $forn_id = NULL;
                $clie_id = NULL;

                if($clie_forn['1'] == ' FORN') {
                    $forn_id = $this->input->post('clie_receita_id');
                } else {
                    $clie_id = $this->input->post('clie_receita_id');
                }
                
                $data[$contData] = array(
                    'financeiro_descricao' => ucfirst(set_value('descricao')),
                    'financeiro_valor' => $valores['valor'], // 1º formaPagamento                    
                    'data_vencimento' => $vencimento, // 1º formaPagamento
                    'data_pagamento' => $recebimento != null ? $recebimento : date('Y-m-d'), // 1º formaPagamento
                    'financeiro_baixado' => $recebido, // 1º formaPagamento

                    'financeiro_forn_clie' => set_value('clie_receita'),
                    'financeiro_forn_clie_id'   => $clie_id,
                    'financeiro_forn_clie_id' => $forn_id, 

                    'financeiro_forma_pgto' => $valores['formaPgto'], // 1º formaPagamento
                    'financeiro_tipo' => $this->input->post('tipo'),
                    'categoria_fin_id' => $this->input->post('categoria') 

                );

                // echo "<pre>";
                // var_dump($data[$contData]);die();

                $retorno[$contData] = $this->Financeiro_model->add('financeiro',$data[$contData]);

                $contData++;

            }

            if ($retorno){
                $this->session->set_flashdata('success','Receita adicionada com sucesso!');
                redirect($urlAtual);

            } else {

                $this->data['custom_error'] = '<div class="form_error"><p>Ocorreu um erro.</p></div>';
                redirect($urlAtual);

            }
        }   

    }

   public function adicionarDespesa() {

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        $urlAtual = $this->input->post('urlAtual');

        $this->form_validation->set_rules('fornecedor', '', 'trim|required');
        if ($this->form_validation->run('despesa') == false) {

          $this->session->set_flashdata('erro',validation_errors());
          redirect($urlAtual);

        } else {

            $vencimento = $this->input->post('vencimento');
            $pagamento = $this->input->post('pagamento');

            if (empty($pagamento)) {
                $pagamento = '00/00/0000';
            }else{
                $pagamento = $pagamento = explode('/',  $this->input->post('pagamento'));
                $pagamento = $pagamento = $pagamento[2].'-'.$pagamento[1].'-'.$pagamento[0];
            }

            if($vencimento == null){
                $vencimento = date('d/m/Y');
            }


            try {             

                $vencimento = explode('/', $vencimento);
                $vencimento = $vencimento[2].'-'.$vencimento[1].'-'.$vencimento[0];

            } catch (Exception $e) {
               $vencimento = date('Y/m/d'); 
            }

            if ($this->input->post('pago') == NULL) { 
            	$pago = 0 ;
            }else { 
            	$pago = $this->input->post('pago');
            }  

            /* NOVA ROTINA -- INICIO -- */
            $cont = $this->input->post('qtdFormaPagamentoDes');
            $valorTotal = 0;

            //var_dump($cont);die();

            for ($i=1; $i <= $cont; $i++) { 

                if($i != 1){
                    $dados[$i]['valor']      = floatval($this->input->post('valor'.$i));
                    $dados[$i]['vencimento'] = $this->input->post('vencimento'.$i);
                    $dados[$i]['formaPgto']  = $this->input->post('formaPgto'.$i);
                }else{
                    $dados[$i]['valor']      = floatval($this->input->post('valor'));
                    $dados[$i]['vencimento'] = $this->input->post('vencimento');
                    $dados[$i]['formaPgto']  = $this->input->post('formaPgto');
                }

                $valorTotal += $dados[$i]['valor'];
            }

            $contData = 0;


            foreach ($dados as $valores) {              

                try {
                    
                    $vencimento = explode('/', $valores['vencimento']);
                    $vencimento = $vencimento[2].'-'.$vencimento[1].'-'.$vencimento[0];
                    
                   
                } catch (Exception $e) {
                   $vencimento = date('Y/m/d'); 
                  
                }

                $clie_forn = explode('|', set_value('fornecedor'));
                //var_dump($clie_forn);die();

                $forn_id = NULL;
                $clie_id = NULL;

                if($clie_forn['1'] == ' FORN') {
                    $forn_id = $this->input->post('clie_forn_despesa_id');
                } else {
                    $clie_id = $this->input->post('clie_forn_despesa_id');
                }      

                
                $data[$contData] = array(
                    'financeiro_descricao' => ucfirst(set_value('descricao')),
                    'financeiro_valor' => $valores['valor'], // 1º formaPagamento                    
                    'data_vencimento' => $vencimento, // 1º formaPagamento
                    'data_pagamento' => $pagamento != null ? $pagamento : date('Y-m-d'), // 1º formaPagamento
                    'financeiro_baixado' => $pago, // 1º formaPagamento

                    'financeiro_forn_clie' => set_value('fornecedor'),
                    // 'clientes_id' => $this->input->post('clie_forn_despesa_id'),
                    'financeiro_forn_clie_id' => $clie_id,
                    'financeiro_forn_clie_id' => $forn_id, 

                    'financeiro_forma_pgto' => $valores['formaPgto'], // 1º formaPagamento
                    'financeiro_tipo' => $this->input->post('tipo'),
                    'categoria_fin_id' => $this->input->post('categoria') 
                );


                $retorno[$contData] = $this->Financeiro_model->add('financeiro',$data[$contData]);

                $contData++;

            }

            if ($retorno){

                $this->session->set_flashdata('success','Despesa adicionada com sucesso!');

                redirect($urlAtual);

            } else {

                $this->session->set_flashdata('error','Ocorreu um erro ao tentar adicionar despesa!');

                redirect($urlAtual);

            }

        } 

    }

   public function editar(){   

        $this->load->library('form_validation');
        $this->data['custom_error'] = '';
        $urlAtual = $this->input->post('urlAtual');


        $this->form_validation->set_rules('descricao', '', 'trim|required');
        $this->form_validation->set_rules('fornecedor', '', 'trim|required');
        $this->form_validation->set_rules('valor', '', 'trim|required');
        $this->form_validation->set_rules('vencimento', '', 'trim|required');
        $this->form_validation->set_rules('pagamento', '', 'trim');


        if ($this->form_validation->run() == false) {
          $this->session->set_flashdata('erro',validation_errors());
          redirect($urlAtual);
        } else {

            $vencimento = $this->input->post('vencimento');
            $pagamento = $this->input->post('pagamento');



            try {                

                $vencimento = explode('/', $vencimento);
                $vencimento = $vencimento[2].'-'.$vencimento[1].'-'.$vencimento[0];
                $pagamento = explode('/', $pagamento);
                $pagamento = $pagamento[2].'-'.$pagamento[1].'-'.$pagamento[0];

            } catch (Exception $e) {
               $vencimento = date('Y/m/d'); 
            }

            $data = array(
                'financeiro_descricao' => $this->input->post('descricao'),
                'financeiro_valor' => $this->input->post('valor'),
                'data_vencimento' => $vencimento,
                'data_pagamento' => $pagamento,
                'financeiro_baixado' => $this->input->post('pago'),
                'financeiro_forn_clie' => $this->input->post('fornecedor'),
                'financeiro_forma_pgto' => $this->input->post('formaPgto'),
                'financeiro_tipo' => $this->input->post('tipo'),
                'financeiro_bandeira_cart' => $this->input->post('bandeiraCart'),
                'financeiro_autorizacao_NSU' => $this->input->post('autorizacao_nsu'),
               //  'clientes_id' => $this->input->post('clientes_id')

            );
            

            if ($this->Financeiro_model->edit('financeiro',$data,'idFinanceiro',$this->input->post('id')) == TRUE) {
                $this->session->set_flashdata('success','lançamento editado com sucesso!');
                redirect($urlAtual);
            } else {
            	die('aqui');
                $this->session->set_flashdata('error','Ocorreu um erro ao tentar editar lançamento!');
                redirect($urlAtual);
            }

        }

    }

	public function visualizar()
	{
		$this->load->model('Sistema_model');
		
		$dadosView['dados']   = $this->Financeiro_model->listarId($this->uri->segment(3));
		$dadosView['meio']    = 'financeiro/visualizar';
		$this->load->view('tema/layout',$dadosView);
	}

    public function excluirLancamento(){   

    	$id = $this->input->post('id');


    	if($id == null || ! is_numeric($id)){
    		$json = array('result'=>  false);
    		echo json_encode($json);
    	}
    	else{

    		$result = $this->Financeiro_model->delete('financeiro','idFinanceiro',$id); 
    		if($result){
    			$json = array('result'=>  true);
    			echo json_encode($json);
    		}
    		else{

    			$json = array('result'=>  false);
    			echo json_encode($json);
    		}    		

    	}

    }

	public function autoCompleteClienteFornecedor(){

        $termo = strtolower($this->input->get('term'));
        $this->Financeiro_model->autoCompleteClienteFornecedor($termo);

    }

    public function autoCompleteCliente(){

        $termo = strtolower($this->input->get('term'));
        $this->Financeiro_model->autoCompleteCliente($termo);

    }


        protected function getThisYear() {


        $dias = date("z");
        $primeiro = date("Y-m-d", strtotime("-".($dias)." day"));
        $ultimo = date("Y-m-d", strtotime("+".( 364 - $dias)." day"));
        return array($primeiro,$ultimo);

    }


    protected function getThisWeek(){
        return array(date("Y/m/d", strtotime("last sunday", strtotime("now"))),date("Y/m/d", strtotime("next saturday", strtotime("now"))));
    }

    protected function getLastSevenDays() {
        return array(date("Y-m-d", strtotime("-7 day", strtotime("now"))), date("Y-m-d", strtotime("now")));
    }



    protected function getThisMonth(){

        $mes = date('m');
        $ano = date('Y'); 
        $qtdDiasMes = date('t');
        $inicia = $ano."-".$mes."-01";
        $ate = $ano."-".$mes."-".$qtdDiasMes;

        return array($inicia, $ate);

    }

}
