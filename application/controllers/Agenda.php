<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
		$this->load->model('Agenda_model');
	}

	public function index()
	{
        $dadosView['instrutor'] = $this->Agenda_model->instrutor();
		$dadosView['meio'] = 'agenda/adicionar';
		$this->load->view('tema/layout',$dadosView);	
	}

   public function adicionar(){

	    $this->form_validation->set_rules('semana', 'Semana', 'trim|required');   
        $this->form_validation->set_rules('funcionario', 'Instrutor', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

             $instrutor = $this->input->post('funcionario');
             $semana = $this->input->post('semana');
             $semana = explode(" - ", $semana);
             $semana_inicio = $semana[0];
             $semana_fim = $semana[1];


            $validacaoSemanal = $this->Agenda_model->validacaoSemanal($instrutor, $semana_inicio);

	            if($validacaoSemanal != NULL){
	              $this->session->set_flashdata('success','Já existe mapa com esse instrutor, caso queira editar o mapa.');
		          redirect('agenda/editar/'.$validacaoSemanal[0]->agenda_id); 
				}


        	$dados = array(        		 				 
				  'semana_inicio'            => $semana_inicio,
				  'semana_fim'               => $semana_fim,
				  'instrutor'                => $instrutor,
	  				  
        	);
        

            if (is_numeric($id = $this->Agenda_model->adicionar($dados)) ) {
                $this->session->set_flashdata('success','Registro adicionado com sucesso, monte o mapa.');
                $this->Agenda_model->adicionarHorarioPrimeiro($id);

                redirect('agenda/editar/'.$id);
      
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        } 
        
        $dadosView['instrutor'] = $this->Agenda_model->instrutor();
		$dadosView['meio'] = 'agenda/adicionar';
		$this->load->view('tema/layout',$dadosView);

   }

   public function editar($id)
	{

        $semana = $this->Agenda_model->getSemana($id);
        $semanaDia = date("Y-d-m", strtotime($semana[0]->semana_inicio));
        $dadosView['dados']  = $this->Agenda_model->listarId($id);
		$dadosView['d1'] = date("d/m", strtotime($semanaDia." + 1 day"));
		$dadosView['d2'] = date("d/m", strtotime($semanaDia." + 2 day"));
		$dadosView['d3'] = date("d/m", strtotime($semanaDia." + 3 day"));
		$dadosView['d4'] = date("d/m", strtotime($semanaDia." + 4 day"));
		$dadosView['d5'] = date("d/m", strtotime($semanaDia." + 5 day"));
		$dadosView['d6'] = date("d/m", strtotime($semanaDia." + 6 day"));
	    $dadosView['meio']    = 'agenda/editar';
		$this->load->view('tema/layout',$dadosView);

	}

   public function editarExe()
	{

		$dias = [];
        for ($i=1; $i <= 6; $i++) { 
           $dia = array();
        	  for ($j=1; $j <= 12 ; $j++) { 
        	  	if ($i == 6 && $j > 6){
        	  		array_push($dia, NULL );	
        	  	} else {
	        		array_push($dia, $_POST[$i.'_'.$j]);
        	  	}
        	  }
        	array_push($dias, $dia);
        }

      $contador = intval($this->input->post('contador'));

      foreach ($dias as $key) {
      		
	      	$dados = array(                                       
		        '`01`'                  => $key['0'],
		        '`02`'                  => $key['1'],                  
		        '`03`'                  => $key['2'],
		        '`04`'                  => $key['3'],
		        '`05`'                  => $key['4'],
		        '`06`'                  => $key['5'],
		        '`07`'                  => $key['6'],
		        '`08`'                  => $key['7'],        
		        '`09`'                  => $key['8'],
		        '`10`'                  => $key['9'],
		        '`11`'                  => $key['10'],
		        '`12`'                  => $key['11']

	         );

            $resultado = $this->Agenda_model->editar($dados,$this->input->post('id_agenda'),$contador);

            if($resultado){
                $this->session->set_flashdata('success','Registro editado com sucesso!');
            }else{
                $this->session->set_flashdata('erro','Erro ao editar o registro!');
            }
	    $contador++; 
      }

     redirect('agenda/editar/'.$this->input->post('id_agenda'));

	}

}
