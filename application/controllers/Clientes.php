<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
		$this->load->model('Clientes_model');
	}

	public function index()
	{
		$dadosView['dados'] = $this->Clientes_model->listar();

		$dadosView['meio'] = 'clientes/listar';
		$this->load->view('tema/layout',$dadosView);	
	}

	public function adicionar()
	{
	    $this->form_validation->set_rules('cliente_cpf_cnpj', 'CPF', 'trim|required|is_unique[clientes.cliente_cpf_cnpj]');
	    $this->form_validation->set_rules('cliente_celular', 'Celular', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {
       	
        	
        	$dados = array(        		 				 
				  'cliente_cpf_cnpj'            => $this->input->post('cliente_cpf_cnpj'),
				  'cliente_tipo'                => $this->input->post('cliente_tipo'),
				  'cliente_nome'                => $this->input->post('cliente_nome'),
				  'cliente_endereco'            => $this->input->post('cliente_endereco'),
				  'cliente_numero' 				=> $this->input->post('cliente_numero'),
				  'cliente_complemento' 		=> $this->input->post('cliente_complemento'),
				  'cliente_bairro' 				=> $this->input->post('cliente_bairro'),				  
				  'cliente_estado' 				=> $this->input->post('cliente_estado'),
				  'cliente_cidade' 				=> $this->input->post('cliente_cidade'),
				  'cliente_cep' 				=> $this->input->post('cliente_cep'),
				  'cliente_telefone' 			=> $this->input->post('cliente_telefone'),
				  'cliente_celular' 			=> $this->input->post('cliente_celular'),
				  'cliente_data_cadastro' 		=> date('Y-m-d'),
				  'cliente_email' 				=> $this->input->post('cliente_email'),	
				  'cliente_ativo' 				 => $this->input->post('cliente_ativo'),
				  'cliente_limite_cred_cliente'  => $this->input->post('cliente_limite_cred_cliente'),
				  'cliente_limite_dias_cliente'  => $this->input->post('cliente_limite_dias_cliente'),			  				  
        	);
        
			// var_dump($dados);die();

        	$resultado = $this->Clientes_model->adicionar($dados);

        	if($resultado){
        		$this->session->set_flashdata('success','Registro adicionado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        }

		$dadosView['estados'] = $this->Clientes_model->pegarEstados();
		$dadosView['perfil']   = $this->Clientes_model->pegarPerfil();
		$dadosView['lojas'] = $this->Clientes_model->getAllFilias();

		$dadosView['meio'] = 'clientes/adicionar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function editar()
	{
		$this->form_validation->set_rules('cliente_cpf_cnpj', 'CPF', 'trim|required');
		$this->form_validation->set_rules('cliente_celular', 'Celular', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {


        	$dados = array(        		 				 
				  'cliente_cpf_cnpj'            => $this->input->post('cliente_cpf_cnpj'),
				  'cliente_tipo'                => $this->input->post('cliente_tipo'),
				  'cliente_nome'                => $this->input->post('cliente_nome'),
				  'cliente_endereco'            => $this->input->post('cliente_endereco'),
				  'cliente_numero' 				=> $this->input->post('cliente_numero'),
				  'cliente_complemento' 		=> $this->input->post('cliente_complemento'),
				  'cliente_bairro' 				=> $this->input->post('cliente_bairro'),	

				  'cliente_estado' 				=> $this->input->post('cliente_estado'),
				  'cliente_cidade' 				=> $this->input->post('cliente_cidade'),
				  
				  'cliente_cep' 				=> $this->input->post('cliente_cep'),
				  'cliente_telefone' 			=> $this->input->post('cliente_telefone'),
				  'cliente_celular' 			=> $this->input->post('cliente_celular'),
				  'cliente_email' 				=> $this->input->post('cliente_email'),	
				  'cliente_ativo' 				 => $this->input->post('cliente_ativo'),
				  'cliente_limite_cred_cliente'  => $this->input->post('cliente_limite_cred_cliente'),
				  'cliente_limite_dias_cliente'  => $this->input->post('cliente_limite_dias_cliente'),			  				 				  
        	);

        	// var_dump($dados);die();
        
        	$resultado = $this->Clientes_model->editar($dados,$this->input->post('id'));

        	if($resultado){
        		$this->session->set_flashdata('success','Registro editado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao editado o registro!');
        	}
        }

        $this->load->model('Sistema_model');

        $dadosView['dados']   = $this->Clientes_model->listarId($this->uri->segment(3));
		$dadosView['perfil']   = $this->Clientes_model->pegarPerfil();
		$dadosView['estados'] = $this->Clientes_model->pegarEstados();
		$dadosView['lojas'] = $this->Clientes_model->getAllFilias();
		$dadosView['cidades'] = $this->Sistema_model->selecionarCidades($dadosView['dados'][0]->cliente_estado);
		$dadosView['meio']    = 'clientes/editar';

		$this->load->view('tema/layout',$dadosView);

	}

	public function visualizar()
	{
		$this->load->model('Sistema_model');
		
		$dadosView['dados']   = $this->Clientes_model->listarId($this->uri->segment(3));
		$dadosView['nivel']   = $this->Clientes_model->pegarPerfil();
		$dadosView['estados'] = $this->Clientes_model->pegarEstados();
		$dadosView['cidades'] = $this->Sistema_model->selecionarCidades($dadosView['dados'][0]->cliente_estado);

		$dadosView['meio']  = 'clientes/visualizar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function excluir()
	{
		$id = $this->uri->segment(3);

		$dados  = array(
						'cliente_visivel' => 0					
					  );
		$resultado = $this->Clientes_model->excluir($dados,$id);

		if($resultado){
			$this->session->set_flashdata('success','registro excluidos com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir o registro!');
		}

		redirect('Clientes','refresh');
	}

	public function autoCompleteClientes()
	{
		$termo = strtolower($this->input->get('term'));
        $this->Clientes_model->autoCompleteClientes($termo);
	}

	public function wscnpj()
    {
        $caracteres = array(".","/","-");
        $cpfCnpj  = $this->input->post('documento');        
        $cpfCnpj = str_replace($caracteres, "", $cpfCnpj);       
        $json = file_get_contents('https://receitaws.com.br/v1/cnpj/'.$cpfCnpj);
        
        echo $json;
    }


}
