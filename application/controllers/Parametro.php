<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parametro extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
		$this->load->model('Parametro_model');
	}

	public function index()
	{
		// $dadosView['dados'] = $this->Parametro_model->listar();

		// $dadosView['meio'] = 'parametro/listar';
		// $this->load->view('tema/layout',$dadosView);	

		redirect('parametro/editar','refresh');
	}

	

	public function editar()
	{
		$this->form_validation->set_rules('parametro_cart_debito', 'Descrição', 'trim|required');
		$this->form_validation->set_rules('parametro_cart_credito', 'Descrição', 'trim|required');		
				
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$dados = array(        		 
				'parametro_cart_debito'          => $this->input->post('parametro_cart_debito'),
				'parametro_cart_credito'         => $this->input->post('parametro_cart_credito')
							  
        	);
     
        	$resultado = $this->Parametro_model->editar($dados,$this->input->post('id'));

        	if($resultado){
        		$this->session->set_flashdata('success','Registro editado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao editar o registro!');
        	}
        }        

        $dadosView['dados']   = $this->Parametro_model->listarId(1);
		$dadosView['meio']    = 'parametro/editar';

		$this->load->view('tema/layout',$dadosView);

	}

   public function updateMassa()
	{
		$this->form_validation->set_rules('parametro_cart_debito', 'Descrição', 'trim|required');
		$this->form_validation->set_rules('parametro_cart_credito', 'Descrição', 'trim|required');		
				
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {
       		 
				$debito = $this->input->post('parametro_cart_debito');
				$credito = $this->input->post('parametro_cart_credito');
							  
     
        	$resultado = $this->Parametro_model->updateMassa($debito, $credito);

        	if($resultado){
        		$this->session->set_flashdata('success','Update feito com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro no Update!');
        	}
        }        

        $dadosView['dados']   = $this->Parametro_model->listarId(1);
		$dadosView['meio']    = 'parametro/editar';

		$this->load->view('tema/layout',$dadosView);

	}
	
}
