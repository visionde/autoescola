<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Niveis extends CI_Controller {

		public function __construct()
	{   
		parent::__construct();
		$this->load->model('Niveis_model');
	}

	public function index()
	{
		$dadosView['dados'] = $this->Niveis_model->listar();

		$dadosView['meio'] = 'niveis/listar';
		$this->load->view('tema/layout',$dadosView);	
	}

	public function adicionar()
	{
	    $this->form_validation->set_rules('nivel', 'Nível', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$dados = array(        		 
				  'nivel'                 => $this->input->post('nivel'),
				  'visivel'               => 1,
				  'data_atualizacao'      => date('Y-m-d H:i:s'),	
        	);

        	$resultado = $this->Niveis_model->adicionar($dados);

        	if($resultado){
        		$this->session->set_flashdata('success','Registro adicionado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao adicionado o registro!');
        	}
        }

		$dadosView['meio'] = 'niveis/adicionar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function editar()
	{
		$this->form_validation->set_rules('nivel', 'Nivel', 'trim|required');
       
        if($this->form_validation->run() == FALSE)
		{
        	$this->session->set_flashdata('erro',validation_errors());
        } else {

        	$dados = array(        		 
				  'nivel'                 => $this->input->post('nivel'),
				  'visivel'               => 1,
				  'data_atualizacao'      => date('Y-m-d H:i:s'),	
        	);
     
        	$resultado = $this->Niveis_model->editar($dados,$this->input->post('id'));

        	if($resultado){
        		$this->session->set_flashdata('success','Registro editado com sucesso!');
        	}else{
        		$this->session->set_flashdata('erro','Erro ao editar o registro!');
        	}
        }

        $this->load->model('Sistema_model');

        $dadosView['dados']   = $this->Niveis_model->listarId($this->uri->segment(3));
		$dadosView['meio']    = 'niveis/editar';

		$this->load->view('tema/layout',$dadosView);

	}

	public function visualizar()
	{
		$this->load->model('Sistema_model');
		
		$dadosView['dados']   = $this->Niveis_model->listarId($this->uri->segment(3));
		$dadosView['meio']    = 'niveis/visualizar';
		$this->load->view('tema/layout',$dadosView);
	}

	public function excluir()
	{
		$id = $this->uri->segment(3);

		$dados  = array(
						'visivel' => 0,
						'data_atualizacao' => date('Y-m-d H:i:s')
					  );
		$resultado = $this->Niveis_model->excluir($dados,$id);

		if($resultado){
			$this->session->set_flashdata('success','registro excluidos com sucesso!');
		}else{
			$this->session->set_flashdata('erro','Erro ao excluir o registro!');
		}

		redirect('Niveis','refresh');
	}

	public function permissoes()
	{
		$permissoesNivel = $this->input->post();

		if($permissoesNivel){

			unset($permissoesNivel['id']);

			$permissoes = array();

			foreach ($permissoesNivel as $key => $value) {
				$permissoes[$key] = 1;
			}
			$dados['permissoes'] = serialize($permissoes);

			$resultado = $this->Niveis_model->editar($dados,$this->input->post('id'));

	    	if($resultado){

	    		if($this->input->post('id') == $this->session->userdata('usuario_nivel')){
	    			$this->session->set_userdata($dados);
	    		}
	    		
	    		$this->session->set_flashdata('success','Registro adicionado com sucesso!');
	    	}else{
	    		$this->session->set_flashdata('erro','Erro ao adicionar o registro!');
	    	}

	    	redirect('Niveis','refresh');

	    }else{

	    	$this->load->model('Sistema_model');
			
			$dadosView['dados']   = $this->Niveis_model->listarId($this->uri->segment(3));
			$dadosView['meio']    = 'niveis/permissoes';
			$this->load->view('tema/layout',$dadosView);
	    }

		
	}

}
