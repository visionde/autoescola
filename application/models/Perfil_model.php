<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil_model extends CI_Model {
	
	public $tabela  = "perfis";
	public $visivel = "perfil_visivel";
	public $chave   = "perfil_id";

	public function listar()
	{
		$this->db->select("*");
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function listarId($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}


	public function adicionar($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function editar($dados, $id)
	{
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;
	}

	public function excluir($dados, $id)
	{
		$this->db->where($this->chave,$id);
		$this->db->update($this->tabela,$dados);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function visualizar($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}
}