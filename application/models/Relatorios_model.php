<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios_model extends CI_Model {
	
	######## RELATORIO MENU CLIENTE ########
	public function clientes($mes = false)
	{
		$this->db->select("*");
		$this->db->where('cliente_visivel', 1);


		if($mes){
			$this->db->where('YEAR(cliente_data_cadastro)', date('Y'));
			$this->db->where('MONTH(cliente_data_cadastro)', date('m'));
		}
		return $this->db->get('clientes')->result();
	}

	public function clientesTotal()
	{	
		return $this->db->count_all('clientes');
	}

	public function clientesTotalAnive()
	{	
		$this->db->where('YEAR(cliente_data_cadastro)', date('Y'));
		$this->db->where('MONTH(cliente_data_cadastro)', date('m'));
		$this->db->from('clientes');
		return $this->db->count_all_results();
	}
	######## FIM RELATORIO MENU CLIENTE ########


	######## RELATORIO MENU PRODUTO ########
	public function pegarGrupoProduto()
	{
		$this->db->select('*');
		$this->db->where('categoria_prod_visivel', 1);
		return $this->db->get('categoria_produto')->result();
	}

	public function totalProduto()
	{
		return $this->db->count_all('produto');
	}

	public function totalEstoqueMinimo()
	{


		$this->db->where('produto_estoque < produto_estoque_minimo');
		$this->db->where('produto_visivel',1);
		$this->db->from('produto');
		return $this->db->count_all_results();

	}

	public function totalValorEstoque()
	{		
		$this->db->select('  SUM(`produto_preco_custo` * `produto_estoque`) as totalValor ');		
		return $this->db->get('produto')->result();
	}


	public function produtos()
	{
		$this->db->order_by('produto_descricao','asc');
        $this->db->where('produto_visivel',1);
        return $this->db->get('produto')->result();
	}

	public function estoqueMinimo()
	{
		$this->db->order_by('produto_descricao','asc');
  		$this->db->where('produto_estoque < produto_estoque_minimo');
  		$this->db->where('produto_visivel',1);
  		return $this->db->get('produto')->result();
	}

	public function valorEstoque()
	{
        $query = "SELECT produto_descricao, produto_estoque, produto_preco_custo, produto_preco_venda, ROUND( (
            (
            produto_preco_venda - produto_preco_custo
            ) * produto_estoque ) , 2
            ) AS lucro                    
            FROM `produto`  WHERE produto_estoque > 0 and produto_visivel = 1";

        return $this->db->query($query)->result();
	}

	public function pegarEtiquetaProduto($grupo)
	{
		$this->db->select('*, "url" ');
		$this->db->where('produto_visivel', 1);
		$this->db->where('produto_categoria_id', $grupo);
		return $this->db->get('`produto`')->result();
	}


	######## FIM RELATORIO MENU PRODUTO ########
}