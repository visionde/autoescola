<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistema_model extends CI_Model {
	/**
	 * [$tabela, $campoChave description] Atributo para facilitar a alteração da classe model com a tabela a ser utilizada
	 * @var string
	 */
	public $tabela = "usuarios";
	public $visivel = "usuario_visivel";
	
	public function processarLogin($dados)
	{
		
		$this->db->select('*');
		$this->db->join('perfis','usuario_perfil = perfil_id');
		$this->db->where('usuario_email',$dados['usuario']);
		$this->db->where('usuario_senha',$dados['senha']);
		$this->db->where($this->tabela.".".$this->visivel,"1");

		return $this->db->get('`emitente,`'.$this->tabela)->result();

	}	

	public function selecionarCidades($estado ='')
	{
		$this->db->select('  `cidades`.`id_cidade`, `cidades`.`nome` ');
		$this->db->join('`estados`','`cidades`.`id_estado` = `estados`.`id_estado`');
		$this->db->where('sigla',$estado);
		return $this->db->get('cidades')->result();
	}


	public function totalUsuarios()
	{
		$this->db->like($this->visivel, '1');
		$this->db->from($this->tabela);
		return $this->db->count_all_results();
	}
	public function totalClientes()
	{
		$this->db->like('cliente_visivel', '1');
		$this->db->from('clientes');
		return $this->db->count_all_results();
	}
	public function totalVendas()
	{
		$this->db->like('venda_visivel', '1');
		$this->db->from('vendas');
		return $this->db->count_all_results();
	}
	public function totalPedidos()
	{
		$this->db->like('pedido_visivel', '1');
		$this->db->from('pedidos');
		return $this->db->count_all_results();
	}

	public function pegarEmitente()
	{
		$this->db->select('*');
		return $this->db->get('emitente')->result();
	}
	
	public function editarEmitente($dados, $id)
	{
		$this->db->where('emitente_id',$id);		
		
		if($this->db->update('emitente',$dados))
		{
			return true;
		}

		return false;
	}

	public function alterarSenha($dados)
	{
		$this->db->where('usuario_id',$this->session->userdata('usuario_id'));		
		
		if($this->db->update('usuarios',$dados))
		{
			return true;
		}

		return false;
	}

	public function pesquisarClientes($pesquisa)
	{
		$this->db->select("cliente_id,
							cliente_nome,
							cliente_cpf_cnpj,
							cliente_telefone,
							cliente_celular,		
							IF(cliente_tipo = 1,'FÍSICA','JURÍDICA') AS tipo");
		
		$this->db->where('cliente_visivel', 1);

		// if(!verificarPermissao('vTudo')){
		// 	$this->db->where('cpf_consultor', $this->session->userdata('usuario_cpf'));
		// }

		$this->db->like('cliente_nome', $pesquisa);
		$this->db->or_like('cliente_cpf_cnpj', $pesquisa);
		return $this->db->get('clientes')->result();
		
	}

	public function pesquisarfornecedores($pesquisa)
	{
		$this->db->select("fornecedor_id,
							fornecedor_nome,
							fornecedor_cnpj_cpf,
							fornecedor_telefone,
							fornecedor_celular,		
							IF(fornecedor_tipo = 1,'FÍSICA','JURÍDICA') AS tipo");
		
		$this->db->where('fornecedor_visivel', 1);

		// if(!verificarPermissao('vTudo')){
		// 	$this->db->where('cpf_consultor', $this->session->userdata('usuario_cpf'));
		// }

		$this->db->like('fornecedor_nome', $pesquisa);
		$this->db->or_like('fornecedor_cnpj_cpf', $pesquisa);
		return $this->db->get('fornecedores')->result();
		
	}

	public function apiEntrada()
	{

		$query = $this->db->query(" SELECT COUNT(*) AS total FROM `movimentacao_produtos` WHERE `movimentacao_produto_status` = 1; ");

	    return $query->result();

	}	

}