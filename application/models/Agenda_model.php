<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agenda_model extends CI_Model {

    public $tabela  = "agenda";
	// public $visivel = "agenda_visivel";
	public $chave   = "agenda_id";
	
	public function instrutor()
	{
		$this->db->select("*");	
		$this->db->where('funcionario_cargo', 1);	
		$this->db->where('funcionario_visivel', 1);
		return $this->db->get('funcionarios')->result();
	}

    public function validacaoSemanal($instrutor, $semana_inicio)
	{
		$this->db->select("*");
		$this->db->where('semana_inicio', $semana_inicio);
		$this->db->where('instrutor', $instrutor);
		return $this->db->get($this->tabela)->result();
	}

	public function adicionar($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			//return TRUE;
			return $this->db->insert_id($this->tabela);
		}
		
		return FALSE; 
	}

    public function adicionarHorarioPrimeiro($id)
	{
		for ($i=0; $i <= 5; $i++) {  		
	      	$dados = array(  
		        'agenda_id'           => $id,                                        
		        '`01`'                  => '',
		        '`02`'                  => '',                  
		        '`03`'                  => '',
		        '`04`'                  => '',
		        '`05`'                  => '',
		        '`06`'                  => '',
		        '`07`'                  => '',
		        '`08`'                  => '',        
		        '`09`'                  => '',
		        '`10`'                  => '',
		        '`11`'                  => '',
		        '`12`'                  => ''

	         );

			$this->db->insert('agenda_itens', $dados);

		}

	}

    public function getSemana($id)
	{
		$this->db->select("*");		
		$this->db->where($this->chave, $id);
		return $this->db->get($this->tabela)->result();
	}

	 public function listarId($id)
	{
		$this->db->select("*");		
		$this->db->where('agenda_id', $id);
		return $this->db->get('agenda_itens')->result_array();
	}

	public function editar($dados, $id, $idItens)
	{
		$this->db->where('agenda_id',$id);	
		$this->db->where('agenda_idItens',$idItens);		
		
		if($this->db->update('agenda_itens',$dados))
		{
			return true;
		}

		return false;
	}





}