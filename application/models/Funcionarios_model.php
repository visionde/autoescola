<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funcionarios_model extends CI_Model {
	
	public $tabela  = "funcionarios";
	public $visivel = "funcionario_visivel";
	public $chave   = "funcionario_id";

	public function listar()
	{
		$this->db->select("*");
		$this->db->join('cargos','`funcionario_cargo` = `cargo_id`','LEFT');
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function listarId($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}


	public function adicionar($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function editar($dados, $id)
	{
		$this->db->where($this->chave,$id);		
		
		if($this->db->update($this->tabela,$dados))
		{
			return true;
		}

		return false;
	}

	public function excluir($dados, $id)
	{
		$this->db->where($this->chave,$id);
		$this->db->update($this->tabela,$dados);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function visualizar($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function pegarEstados()
	{
		$this->db->select('*');
		return $this->db->get('estados')->result();
	}

	public function pegarEstadoId($sigla)
	{
		$this->db->select(' id_estado ');
		$this->db->where('sigla', $sigla);
		return $this->db->get('estados')->result();
	}

	public function pegarCidadeId($estado,$cidade)
	{
		$this->db->select(' id_cidade ');
		$this->db->where('nome', $cidade);
		$this->db->where('id_estado', $estado);
		return $this->db->get('cidades')->result();
	}

	public function pegarCargos($value='')
	{
		$this->db->select('*');
		return $this->db->get('cargos')->result();
	}

	public function autoCompleteFuncionarios($termo)
	{
		$this->db->select('*');
		$this->db->join('cargos','cargo_id = funcionario_cargo');
        $this->db->limit(5);
        $this->db->like('funcionario_nome', $termo);
        $this->db->where($this->visivel, 1);
        $query = $this->db->get($this->tabela)->result();

        if(count($query) > 0){
            foreach ($query as $row){
                $row_set[] = array('label'=>$row->funcionario_nome.' | CARGO: '.$row->cargo_descricao,'id'=>$row->funcionario_id);
            }
            echo json_encode($row_set);
        }
	}

}