<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Clientes_model extends CI_Model {
	
	public $tabela  = "clientes";
	public $visivel = "cliente_visivel";
	public $chave   = "cliente_id";

	public function listar()
	{
		$this->db->select("*");		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function listarId($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}


	public function adicionar($dados)
	{
		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_megacell_%' ";
        $bases = $this->db->query($sql)->result();;

        foreach ($bases as $b) {
        	$this->db->insert($b->schema_name.'.'.$this->tabela, $dados);
        }

        if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
			
			return FALSE;
	}

	public function editar($dados, $id)
	{
		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_megacell_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {
			$this->db->where($this->chave,$id);	
			$this->db->update($b->schema_name.'.'.$this->tabela,$dados);

        }

	    if($this->db->trans_status() === true){
	        $this->db->trans_commit();
	        return true;
	    }else{
	        $this->db->trans_rollback();
	        return false;
	    }
	}

	public function excluir($dados, $id)
	{
		$sql = "SELECT schema_name FROM information_schema.schemata WHERE schema_name LIKE '%_megacell_%' ";
        $bases = $this->db->query($sql)->result();;
        
        foreach ($bases as $b) {
			$this->db->where($this->chave,$id);	
			$this->db->update($b->schema_name.'.'.$this->tabela,$dados);

        }

	    if($this->db->trans_status() === true){
	        $this->db->trans_commit();
	        return true;
	    }else{
	        $this->db->trans_rollback();
	        return false;
	    }
	}

	public function visualizar($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function pegarEstados()
	{
		$this->db->select('*');
		return $this->db->get('estados')->result();
	}

	public function pegarEstadoId($sigla)
	{
		$this->db->select(' id_estado ');
		$this->db->where('sigla', $sigla);
		return $this->db->get('estados')->result();
	}

	public function pegarCidadeId($estado,$cidade)
	{
		$this->db->select(' id_cidade ');
		$this->db->where('nome', $cidade);
		$this->db->where('id_estado', $estado);
		return $this->db->get('cidades')->result();
	}

	public function pegarPerfil($value='')
	{
		$this->db->select('*');
		return $this->db->get('perfis')->result();
	}

	public function autoCompleteClientes($termo)
	{
		$this->db->select('*');
        $this->db->limit(5);
        $this->db->like('cliente_nome', $termo);
        $this->db->where($this->visivel, 1);
        $query = $this->db->get($this->tabela)->result();

        if(count($query) > 0){
            foreach ($query as $row){
                $row_set[] = array('label'=>$row->cliente_nome.' | TEL: '.$row->cliente_telefone,'id'=>$row->cliente_id);
            }
            echo json_encode($row_set);
        }
	}
	public function getAllFilias()
    {
        $sql = "SHOW DATABASES";

        $result = $this->db->query($sql);

        $bases = array();
        if ($result->num_rows() > 0) {
            foreach (($result->result_array()) as $row) {
              if (strpos($row['Database'], '_megacell_') !== false && $row['Database'] !=
                'wdm_clientes') {
              	$lojas = explode("_megacell_", $row['Database']);
                 
                   $bases[]['lojas'] = $lojas[1];
                
              }
            }

          return $bases;
        }
        return FALSE;
    }

}