<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financeiro_model extends CI_Model {
	
	public $tabela  = "financeiro";
	public $visivel = "financeiro_visivel";
	public $chave   = "idFinanceiro";

	public function listar($page, $itens_per_page, $pesquisa = NULL)
	{
        $query =  "SELECT * FROM financeiro";   
   		if($pesquisa){

            $query .= " WHERE (".$pesquisa.")";

          }
	    $query .= " LIMIT ".(($page - 1) * $itens_per_page).", ".$itens_per_page.";";
		// echo "<pre>";
		// echo ($query);
		// echo "</pre>";
		return $this->db->query($query)->result();

	}

   public function contador($page, $pesquisa = NULL)
	{
		$query =  "SELECT 
		COUNT(`financeiro`.`idFinanceiro`) AS total
		FROM `financeiro`"; 
		if($pesquisa){

            $query .= " WHERE (".$pesquisa.")";

        }
		// echo "<pre>";
		// exit($query);
        return $this->db->query($query)->result();

    }


	public function listarId($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}


	public function add($table,$data){

        $this->db->insert($table, $data);         

        if ($this->db->affected_rows() == '1')

		{
			return TRUE;
		}	

		return FALSE;     

    }   

	public function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);


        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}	

		return FALSE;   
    }


    function delete($table,$fieldID,$ID){

        $this->db->where($fieldID,$ID);

        $this->db->delete($table);

        if ($this->db->affected_rows() == '1')

		{
			return TRUE;
		}		

		return FALSE;    

    }

	public function visualizar($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}
	
	public function getAllTiposCartao()
	{
		$this->db->where('categoria_cart_visivel', 1);
		return $this->db->get('categoria_cartao')->result();
	}

	public function getAllTiposCategorias()
	{
		$this->db->where('categoria_fina_visivel', 1);
		return $this->db->get('categoria_financeiro')->result();
	}

	public function autoCompleteClienteFornecedor($q)
    {   
        $sql = "SELECT `cliente_id` AS id  , `cliente_nome` AS nome, 'CLIE' AS tipo FROM clientes where cliente_nome like '%{$q}%'
                UNION    
                SELECT `fornecedor_id` AS id , `fornecedor_nome` AS nome, 'FORN' AS tipo FROM fornecedores where fornecedor_nome like '%{$q}%' ";
      
        $resultado = $this->db->query($sql)->result();

        if(count($resultado) > 0){
            foreach ($resultado as $row){
                $row_set[] = array('label'=>$row->nome.' | '.$row->tipo,'id'=>$row->id);
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteCliente($q)
    {   
        $sql = "SELECT `cliente_id` AS id  , `cliente_nome` AS nome, 'CLIE' AS tipo FROM clientes where cliente_nome like '%{$q}%' ";
      
        $resultado = $this->db->query($sql)->result();

        if(count($resultado) > 0){
            foreach ($resultado as $row){
                $row_set[] = array('label'=>$row->nome.' | '.$row->tipo,'id'=>$row->id);
            }
            echo json_encode($row_set);
        }
    }
}