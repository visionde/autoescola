<?php 

class Authentic
{

	function checkAuth()
	{
		$this->CI = get_instance();

		$controllerAction =  $this->CI->uri->segment(1);

		if ( $controllerAction <> 'cotacaofornecedor') { 
		
	    	if ($this->CI->session->userdata('usuario_id') == '' && $controllerAction <> 'sistema')
			{
				redirect('sistema','refresh');
			}

		}

	}
}

 ?>