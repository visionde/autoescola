<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


 
require __DIR__.'/gerencianet/vendor/autoload.php'; // caminho relacionado ao Composer
 
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;
 
function inicarTransacao($item_1)
{	

	$CI =&get_instance();

	$CI->load->model('Model_contrato');
	$chaves = $CI->Model_contrato->pegarChavesApi();


	$clientId     = $chaves[0]->client_id; // insira seu Client_Id, conforme o ambiente (Des ou Prod)
	$clientSecret = $chaves[0]->client_secret; // insira seu Client_Secret, conforme o ambiente (Des ou Prod)

	if($chaves[0]->id == 1){
		$options = [
					  'client_id' => $clientId,
					  'client_secret' => $clientSecret,
					  'sandbox' => false // altere conforme o ambiente (true = desenvolvimento e false = producao)
					];
	}else{
		$options = [
					  'client_id' => $clientId,
					  'client_secret' => $clientSecret,
					  'sandbox' => true // altere conforme o ambiente (true = desenvolvimento e false = producao)
					];
	}


	 
	$items =  [
	    $item_1
	];

	//Exemplo para receber notificações da alteração do status da transação.
	//$metadata = ['notification_url'=>'sua_url_de_notificacao_.com.br']
	//Outros detalhes em: https://dev.gerencianet.com.br/docs/notificacoes

	//Como enviar seu $body com o $metadata
	//$body  =  [
	//    'items' => $items,
	//    'metadata' => $metadata
	//];

	$body  =  [
	    'items' => $items
	];

	try {

	    $api = new Gerencianet($options);
	    $charge = $api->createCharge([], $body);

	    if($charge['code'] == 200){
	    	return $charge['data']['charge_id'];
	    }else{
	    	return false;
	    }
	    
	} catch (GerencianetException $e) {
	    print_r($e->code);
	    print_r($e->error);
	    print_r($e->errorDescription);
	} catch (Exception $e) {
	    print_r($e->getMessage());
	}
}

function associarFormaPagamento($charge_id,$customer,$dataVencimento)
{

	$CI =&get_instance();

	$CI->load->model('Model_contrato');
	$chaves = $CI->Model_contrato->pegarChavesApi();


	$clientId     = $chaves[0]->client_id;     // insira seu Client_Id, conforme o ambiente (Des ou Prod)
	$clientSecret = $chaves[0]->client_secret; // insira seu Client_Secret, conforme o ambiente (Des ou Prod)
	 
	if($chaves[0]->id == 1){
		$options = [
					  'client_id' => $clientId,
					  'client_secret' => $clientSecret,
					  'sandbox' => false // altere conforme o ambiente (true = desenvolvimento e false = producao)
					];
	}else{
		$options = [
					  'client_id' => $clientId,
					  'client_secret' => $clientSecret,
					  'sandbox' => true // altere conforme o ambiente (true = desenvolvimento e false = producao)
					];
	}
	

	// $charge_id refere-se ao ID da transação gerada anteriormente
	$params = [
	  'id' => $charge_id
	];
	 
	
	 
	$bankingBillet = [
	  'expire_at' => $dataVencimento, // data de vencimento do boleto (formato: YYYY-MM-DD)
	  'customer' => $customer
	];
	 
	$payment = [
	  'banking_billet' => $bankingBillet // forma de pagamento (banking_billet = boleto)
	];
	 
	$body = [
	  'payment' => $payment
	];
	 
	try {
	    $api = new Gerencianet($options);
	    $charge = $api->payCharge($params, $body);
	 
	    return $charge;
	} catch (GerencianetException $e) {
	    print_r($e->code);
	    print_r($e->error);
	    print_r($e->errorDescription);
	} catch (Exception $e) {
	    print_r($e->getMessage());
	}
}
function cancelar_transacao($charge_id)
{

 
	$CI =&get_instance();

	$CI->load->model('Model_contrato');
	$chaves = $CI->Model_contrato->pegarChavesApi();


	$clientId     = $chaves[0]->client_id;     // insira seu Client_Id, conforme o ambiente (Des ou Prod)
	$clientSecret = $chaves[0]->client_secret; // insira seu Client_Secret, conforme o ambiente (Des ou Prod)

 if($chaves[0]->id == 1){
		$options = [
					  'client_id' => $clientId,
					  'client_secret' => $clientSecret,
					  'sandbox' => false // altere conforme o ambiente (true = desenvolvimento e false = producao)
					];
	}else{
		$options = [
					  'client_id' => $clientId,
					  'client_secret' => $clientSecret,
					  'sandbox' => true // altere conforme o ambiente (true = desenvolvimento e false = producao)
					];
	}


 
// $charge_id refere-se ao ID da transação gerada anteriormente
$params = [
  'id' => $charge_id
];
 
try {
    $api = new Gerencianet($options);
    $charge = $api->cancelCharge($params, []);
 
    print_r($charge);
} catch (GerencianetException $e) {
    print_r($e->code);
    print_r($e->error);
    print_r($e->errorDescription);
} catch (Exception $e) {
    print_r($e->getMessage());
}
}

function cancelar_parcela($dados)
{

 	$CI =&get_instance();

	$CI->load->model('Contratos_model');
	$chaves = $CI->Contratos_model->pegarChavesApi();

	$clientId     = $chaves[0]->client_id;     // insira seu Client_Id, conforme o ambiente (Des ou Prod)
	$clientSecret = $chaves[0]->client_secret; // insira seu Client_Secret, conforme o ambiente (Des ou Prod)

	$ambiente = ($chaves[0]->id == 1)? false:true;
	 
	if($chaves[0]->id == 1){
		$options = [
					  'client_id' => $clientId,
					  'client_secret' => $clientSecret,
					  'sandbox' => $ambiente // altere conforme o ambiente (true = desenvolvimento e false = producao)
					];
	}else{
		$options = [
					  'client_id' => $clientId,
					  'client_secret' => $clientSecret,
					  'sandbox' => $ambiente // altere conforme o ambiente (true = desenvolvimento e false = producao)
					];
	}

	$params = [
	  'id' => $dados['api_carnet_id'],
	  'parcel' => $dados['parcela']
	];

	try {
	    $api = new Gerencianet($options);
	    $response = $api->cancelParcel($params, []);

	    return $response;
	} catch (GerencianetException $e) {
	    print_r($e->code);
	    print_r($e->error);
	    print_r($e->errorDescription);
	} catch (Exception $e) {
	    print_r($e->getMessage());
	}
}
function pagamentoCarne($customer,$dataVencimento,$item_1,$parcelas)
{
	$CI =&get_instance();

	$CI->load->model('Contratos_model');
	$chaves = $CI->Contratos_model->pegarChavesApi();

	$clientId     = $chaves[0]->client_id;     // insira seu Client_Id, conforme o ambiente (Des ou Prod)
	$clientSecret = $chaves[0]->client_secret; // insira seu Client_Secret, conforme o ambiente (Des ou Prod)

	$ambiente = ($chaves[0]->id == 1)? false:true;
	 
	if($chaves[0]->id == 1){
		$options = [
					  'client_id' => $clientId,
					  'client_secret' => $clientSecret,
					  'sandbox' => $ambiente // altere conforme o ambiente (true = desenvolvimento e false = producao)
					];
	}else{
		$options = [
					  'client_id' => $clientId,
					  'client_secret' => $clientSecret,
					  'sandbox' => $ambiente // altere conforme o ambiente (true = desenvolvimento e false = producao)
					];
	}

/*$item_1 = [
    'name' => 'Item 1', // nome do item, produto ou serviço
    'amount' => 1, // quantidade
    'value' => 1000 // valor (1000 = R$ 10,00)
]
 
$item_2 = [
    'name' => 'Item 2' // nome do item, produto ou serviço
    'amount' => 2, // quantidade
    'value' => 2000 // valor (2000 = R$ 20,00)
]*/
 
$items =  [
    $item_1
];
 
/*$customer = [
  'name' => 'Gorbadoc Oldbuck', // nome do cliente
  'cpf' => '94271564656' , // cpf do cliente
  'phone_number' => '5144916523' // telefone do cliente
];*/

// Exemplo para receber notificações da alteração do status do carne.
// $metadata = ['notification_url'=>'sua_url_de_notificacao_.com.br']
// Outros detalhes em: https://dev.gerencianet.com.br/docs/notificacoes

//Como enviar seu $body com o $metadata
//$body = [
// 'items' => $items,
// 'customer' => $customer,
// 'expire_at' => '2020-12-02',
// 'repeats' => 5,
// 'split_items' => false,
// 'metadata' => $metadata
//];

$body = [
  'items'       => $items,
  'customer'    => $customer,
  'expire_at'   => $dataVencimento, // data de vencimento da primeira parcela do carnê
  'repeats'     => $parcelas,       // número de parcelas do carnê
  'split_items' => false
];

try {
    $api = new Gerencianet($options);
    $carnet = $api->createCarnet([], $body);
 
    return $carnet;
} catch (GerencianetException $e) {
	return $e;
} catch (Exception $e) {
    return $e;
}
}