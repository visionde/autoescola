<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
        <?php if(verificarPermissao('aFuncionario')){ ?>
        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
        <?php } ?>
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="example" class="table" width="100%">
          <thead>
            <tr>                                            
              <th>Nome</th>
              <th>Celular</th>
              <th>Cpf</th>
              <th>Cargo</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>

            <?php foreach ($dados as $d){ ?>
            <tr>                      
              <td> <?php echo $d->funcionario_nome;?></td> 
              <td> <?php echo $d->funcionario_celular;?></td> 
              <td> <?php echo $d->funcionario_cpf;?></td>
              <td> <?php echo $d->cargo_descricao;?></td>
              <td>                
                <?php if(verificarPermissao('vFuncionario')){ ?>
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizar/<?php echo $d->funcionario_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search text-success"></i> </a>
                <?php } ?>
                <?php if(verificarPermissao('eFuncionario')  and $d->funcionario_padrao == 0){ ?>
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editar/<?php echo $d->funcionario_id; ?>" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                <?php } ?>
                <?php if(verificarPermissao('dFuncionario') and $d->funcionario_padrao == 0){ ?>
                <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?php echo base_url().$this->uri->segment(1)."/excluir/".$d->funcionario_id; ?>');" ><i class="fa fa-trash text-danger"></i></a>
                <?php } ?>
              </td>
            </tr>
            <?php } ?>

          </tbody>
          <tfoot>
            <tr>                     
              <th>Nome</th>
              <th>Celular</th>
              <th>Cpf</th>
              <th>Função</th>
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>