<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
          <div class="box-body">

            <div class="form-group">
              <label for="funcionario_cpf" class="col-sm-2 control-label">CPF</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="funcionario_cpf" id="cpf" value="<?php echo set_value('funcionario_cpf'); ?>" placeholder="CPF">
              </div>
            </div>

            <div class="form-group">
              <label for="funcionario_nome" class="col-sm-2 control-label">Nome Completo</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="funcionario_nome" name="funcionario_nome" value="<?php echo set_value('funcionario_nome'); ?>" placeholder="Nome Completo">
              </div>
            </div> 

            <div class="form-group">
              <label for="funcionario_matricula" class="col-sm-2 control-label">Matricula</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="funcionario_matricula" name="funcionario_matricula" value="<?php echo set_value('funcionario_matricula'); ?>" placeholder="Matricula">
              </div>
            </div>

            <div class="form-group">
              <label for="funcionario_cep" class="col-sm-2 control-label">CEP</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="cep" name="funcionario_cep" value="<?php echo set_value('funcionario_cep'); ?>" placeholder="Cep">
              </div>
            </div>  

            <div class="form-group">
              <label for="funcionario_endereco" class="col-sm-2 control-label">Endereço</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="endereco" name="funcionario_endereco" value="<?php echo set_value('funcionario_endereco'); ?>" placeholder="Endereço">
              </div>
            </div> 
            
            <div class="form-group">
              <label for="funcionario_bairro" class="col-sm-2 control-label">Bairro</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="bairro" name="funcionario_bairro" value="<?php echo set_value('funcionario_bairro'); ?>" placeholder="Bairro">
              </div>
            </div>

            <div class="form-group">
              <label for="funcionario_estado" class="col-sm-2 control-label">Estado</label>
              <div class="col-sm-5">              
                
                <select class="form-control" name="funcionario_estado" id="estado">
                  <option value="">Selecione</option>
                  <?php foreach ($estados as $valor) { ?>
                    <option value='<?php echo $valor->sigla; ?>'><?php echo $valor->nome; ?> </option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="funcionario_cidade" class="col-sm-2 control-label">Cidade</label>
              <div class="col-sm-5">              
                <select class="form-control" name="funcionario_cidade" id="cidade">
                  <option value="">Escolha um estado</option>                                      
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="funcionario_numero" class="col-sm-2 control-label">Número</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="funcionario_numero" name="funcionario_numero" value="<?php echo set_value('funcionario_numero'); ?>" placeholder="Número">
              </div>
            </div>       

            <div class="form-group">
              <label for="funcionario_complemento" class="col-sm-2 control-label">Complemento</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="funcionario_complemento" name="funcionario_complemento" value="<?php echo set_value('funcionario_complemento'); ?>" placeholder="Complemento">
              </div>
            </div>

            <div class="form-group">
              <label for="funcionario_telefone" class="col-sm-2 control-label">Telefone</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="telefone"  name="funcionario_telefone" value="<?php echo set_value('funcionario_telefone'); ?>" placeholder="Telefone">
              </div>
            </div>

            <div class="form-group">
              <label for="funcionario_celular" class="col-sm-2 control-label">Celular</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="celular" name="funcionario_celular" value="<?php echo set_value('funcionario_celular'); ?>" placeholder="Celular">
              </div>
            </div>

            <div class="form-group">
              <label for="funcionario_email" class="col-sm-2 control-label">E-mail</label>
              <div class="col-sm-5">                
                <input type="text" class="form-control" id="funcionario_email" name="funcionario_email" value="<?php echo set_value('funcionario_email'); ?>" placeholder="E-mail">
              </div>
            </div>            

            <div class="form-group">
              <label for="funcionario_cargo" class="col-sm-2 control-label">Cargos</label>
              <div class="col-sm-5">              
                
                <select class="form-control" name="funcionario_cargo" id="funcionario_cargo">
                  <option value="">Selecione</option>
                  <?php foreach ($cargos as $valor) { ?>
                    <option value='<?php echo $valor->cargo_id; ?>'><?php echo $valor->cargo_descricao; ?> </option>
                  <?php } ?>
                </select>
              </div>
            </div>


          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>