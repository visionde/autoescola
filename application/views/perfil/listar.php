<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="col-md-2">
        <?php if(verificarPermissao('aPerfil')){ ?>
        <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/adicionar" type="button" class="btn btn-block btn-default btn-flat">Adicionar</a>
        <?php } ?>
      </div>          
    </div>
    <div class="box-body">
      <div class="container-fluid">
        <table id="example" class="table" width="100%">
          <thead>
            <tr>                                            
              <th>Código</th>
              <th>Descrição</th>
              <th>Ações</th>
            </tr>
          </thead>
          <tbody>

            <?php foreach ($dados as $d){ ?>
            <tr>                      
              <td> <?php echo $d->perfil_id;?></td> 
              <td> <?php echo $d->perfil_descricao;?></td> 
              <td>
                <?php if(verificarPermissao('vPerfil')){ ?>
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/visualizar/<?php echo $d->perfil_id; ?>" data-toggle="tooltip" title="Visualizar"><i class="fa fa-search  text-success"></i> </a>
                <?php } ?>
                <?php if(verificarPermissao('ePerfil') and $d->perfil_padrao == 0){ ?>
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editar/<?php echo $d->perfil_id; ?>" data-toggle="tooltip" title="Editar"><i class="fa fa-edit"></i> </a>
                <?php } ?>
                <?php if(verificarPermissao('dPerfil') and $d->perfil_padrao == 0){ ?>
                <a style="cursor:pointer;" title="Deletar" data-toggle="modal" data-target="#excluir" onclick="excluir('<?php echo base_url().$this->uri->segment(1)."/excluir/".$d->perfil_id; ?>');" ><i class="fa fa-trash  text-danger"></i></a>
                <?php } ?>
                <?php if(verificarPermissao('pPerfil')){ ?>
                <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/permissoes/<?php echo $d->perfil_id; ?>" data-toggle="tooltip" title="Permissões"><i class="fa fa-gear"></i> </a>
                <?php } ?>
              </td>
            </tr>
            <?php } ?>

          </tbody>
          <tfoot>
            <tr>                     
              <th>Código</th>
              <th>Descrição</th>
              <th>Ações</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</section>