<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Clientes</h3>
<table class="table">
	<thead>
		<tr>			
			<th style="padding: 5px;font-size:10">Cliente</th>			
			<th style="padding: 5px;font-size:10">Telefone</th>
			<th style="padding: 5px;font-size:10">Cidade - Estado</th>
			<th style="padding: 5px;font-size:10">Cpf/Cnpj</th>
			<th style="padding: 5px;font-size:10">Tipo</th>
		</tr>
	</thead>
	<tbody>
		<?php $cont = 0; ?>
		<?php foreach ($dados as $valor){ ?>
			<?php $cont++; $tipo = ($valor->cliente_tipo == 1)? 'Física':'Jurídica'; ?>			
			<tr>				
				<td style="padding: 5px;font-size:10"><?php echo $valor->cliente_nome; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->cliente_telefone; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->cliente_cidade."/".$valor->cliente_estado; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->cliente_cpf_cnpj; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $tipo; ?></td>
			</tr>
		
		<?php } ?> 
	</tbody>
</table>