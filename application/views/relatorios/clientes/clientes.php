
<section class="content">
	<div class="row">
		<div class="col-md-3">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Rápidos</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					
					<div class="small-box bg-green">
						<a href="<?php echo base_url() ?>relatorios/clientes/todos" class="small-box-footer" target="_blank">
						<div class="inner">
							<h3><?php echo $totalcliente; ?></h3>

							<p>Todos os clientes</p>
						</div>
						<div class="icon">
							<i class="ion ion-ios-people"></i>
						</div>
						</a>
					</div>

					<div class="small-box bg-blue">
						<a href="<?php echo base_url() ?>relatorios/clientes/mes" class="small-box-footer" target="_blank">
						<div class="inner">
							<h3><?php echo $totalAniversariante; ?></h3>

							<p>Aniversariantes do mês</p>
						</div>
						<div class="icon">
							<i class="ion ion-calendar"></i>
						</div>
						</a>
					</div>

				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/clientesCustom" method="get">
								<div class="col-md-4">
									<label>Cadastrado de</label>
									<div class="form-group">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerCadastroIncio" placeholder="dd/mm/aaaa">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<label>até</label>
									<div class="form-group">
										<div class="input-group date">
											<div class="input-group-addon">
												<i class="fa fa-calendar"></i>
											</div>
											<input type="text" class="form-control pull-right" id="datepickerCadastroFim" placeholder="dd/mm/aaaa">
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<label>&nbsp;</label>
									<button type="button" class="btn btn-primary btn-block">
										<i class="fa fa-print"></i> Imprimir
									</button>
								</div>
						    </form>
						</div>
					</div>
				</div>
			</div>

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Venda de</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerVendaIncio" placeholder="dd/mm/aaaa">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>até</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerVendaFim" placeholder="dd/mm/aaaa">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Exibir produtos</label>
									<select class="form-control">
										<option>Sim</option>
										<option>Não</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Ordenar por</label>
									<select class="form-control">
										<option>Nome cliente A - Z</option>
										<option>Nome cliente Z - A</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Posição Impressão</label>
									<select class="form-control">
										<option>Retrato</option>
										<option>Paisagem</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row no-print text-center" style="margin-top: 40px">
						<div class="col-md-12">
							<button type="button" class="btn btn-default">
								<i class="fa fa-eraser"></i> Limpar
							</button>
							<button type="button" class="btn btn-primary" style="margin-right: 5px;">
								<i class="fa fa-print"></i> Imprimir
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

