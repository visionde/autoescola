<?php 
    @set_time_limit( 300 );
    ini_set('max_execution_time', 300);
    ini_set('max_input_time', 300);
    ini_set('memory_limit', '512M');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Relatórios</title>


  </head>
  <body> 

<?php if ($tipo == '03') { ?>
    # etiqueta só nome

    <div style="width: 600px;" >
		<?php 
		$cont = 1;                    
		foreach ($etiquetas as $e) { 
		?>
	<div  align="center" style="border: 1px solid #000; float: left; margin: 2px;display: block;width: 290px; height:115px;">
	    <center style="padding: 10px;">
	    	<h3><b><?php echo $e->produto_descricao; ?></b></h3>
	    	<!-- <h3> R$ <?php echo $e->produto_preco_venda; ?></h3> -->
		</center>
	</div>
                         
        <?php 
        if($cont % 16 == 0){ ?>

            <div style="page-break-after:always">  </div>                          
        <?php 
            }
            $cont++;
        }
        ?>
    </div>

<?php } if($tipo == '02'){ ?>
    # etiqueta Qr-Code
    
    <div style="width: 600px;" >
        <?php 
        $cont = 1;                    
        foreach ($etiquetas as $e) { 
        ?>
    <div  align="center" style="border: 1px solid #000; float: left; margin: 2px;display: block;width: 290px; height:115px;">
        <center style="padding: 10px;">
            <h3><b><?php echo $e->produto_descricao; ?></b></h3>
            <h3><?php if (isset($e->url)){ echo '<img src="'.$e->url.'"/>'; } ?></h3>
        </center>
    </div>
                         
        <?php 
        if($cont % 16 == 0){ ?>
            <div style="page-break-after:always">  </div>                          
        <?php 
            }
            $cont++;
        }
        ?>
    </div> 


<?php } if($tipo == '01'){ ?> 
    # etiqueta Cód Barra 

    <div style="width: 600px;" >
        <?php 
        $cont = 1;                    
        foreach ($etiquetas as $e) { 
        ?>
    <div  align="center" style="border: 1px solid #000; float: left; margin: 2px;display: block;width: 290px; height:115px;">
        <center style="padding: 10px;">
            <h3><b><?php echo $e->produto_descricao; ?></b></h3>
            <h3><?php if (isset($e->url)){ echo '<img src="'.$e->url.'"/>'; } ?></h3>
        </center>
    </div>
                         
        <?php 
        if($cont % 16 == 0){ ?>
            <div style="page-break-after:always">  </div>                          
        <?php 
            }
            $cont++;
        }
        ?>
    </div> 

<?php } ?> 


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
  </body>
</html>