<?php 
    @set_time_limit( 300 );
    ini_set('max_execution_time', 300);
    ini_set('max_input_time', 300);
    ini_set('memory_limit', '512M');
?>

<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px">Relatório de Produtos</h3>



<?php if (empty($dados[0]->lucro)) { ?>
	<table class="table">
	<thead>
		<tr>			
			<th style="font-size: 1.2em; padding: 5px">Nome</th>			
			<th style="font-size: 1.2em; padding: 5px">UN</th>
			<th style="font-size: 1.2em; padding: 5px;">Preço Custo</th>
            <th style="font-size: 1.2em; padding: 5px;">Preço Venda</th>
            <th style="font-size: 1.2em; padding: 5px;">Estoque</th>
		</tr>
	</thead>
	<tbody>
		<?php
			$totalProdutos = 0;
	        $totalEstoque = 0;   
       
        foreach ($dados as $valor){ 
			$totalProdutos += $valor->produto_preco_venda;
            $totalEstoque += $valor->produto_estoque;
     
        ?>			
			<tr>				
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_descricao; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_unidade; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_preco_custo; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_preco_venda; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_estoque; ?></td>	
			</tr>
		
		<?php } ?>
		    <tr>
               <td colspan="2" style="text-align: right;"><b><?php echo "Valor Total: ".number_format($totalProdutos, 2, '.', ','); ?></b></td>
               <td colspan="2" style="text-align: right;"><b><?php echo "Quantidade Total: ".$totalEstoque ; ?></b></td>
            </tr> 
	</tbody>
</table>

	
<?php } else { ?>
<!-- RELATORIO PRODUTOS x LUCRO  -->
	<table class="table">
	<thead>
		<tr>			
			<th style="font-size: 1.2em; padding: 5px">Nome</th>			
			<!-- <th style="font-size: 1.2em; padding: 5px">UN</th> -->
			<th style="font-size: 1.2em; padding: 5px">Estoque</th>
			<th style="font-size: 1.2em; padding: 5px;">Preço Custo</th>
            <th style="font-size: 1.2em; padding: 5px;">Preço Venda</th>           
            <th style="font-size: 1.2em; padding: 5px;">Lucro</th>

		</tr>
	</thead>
	<tbody>
		<?php
	        $totalCompra = 0;
	        $totalVenda  = 0;
	        $totalLucro  = 0;
        
        foreach ($dados as $valor){ 		

            $totalCompra += $valor->produto_preco_custo * $valor->produto_estoque;
            $totalVenda  += $valor->produto_preco_venda * $valor->produto_estoque;
            $totalLucro  += $valor->lucro; 
        ?>			
			<tr>				
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_descricao; ?></td>				 
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_estoque; ?></td>
				<!-- <td style="padding: 5px;font-size:10"><?php echo $valor->produto_unidade; ?></td> -->
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_preco_custo; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->produto_preco_venda; ?></td>
				<td style="padding: 5px;font-size:10"><?php echo $valor->lucro; ?></td>		
			</tr>
		
		<?php } ?>
 		    <tr>
               <td colspan="2" style="text-align: right;"><b><?php echo "Total Custo: ".number_format($totalCompra, 2, '.', ','); ?></b></td>
               <td colspan="2" style="text-align: right;"><b><?php echo "Total Venda: ".number_format($totalVenda, 2, '.', ','); ?></b></td>
               <td colspan="2" style="text-align: right;"><b><?php echo "Total Lucro: ".number_format($totalLucro, 2, '.', ','); ?></b></td>
              
            </tr> 
	</tbody>
</table>


<?php } ?>