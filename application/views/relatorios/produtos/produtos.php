
<section class="content">
	<div class="row">
		<div class="col-md-3">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Rápidos</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					
					<a href="<?php echo base_url() ?>relatorios/produtos/todos" class="small-box-footer" target="_blank">
					<div class="small-box bg-green">						
						<div class="inner">
							<h3><?php echo $totalProduto; ?></h3>

							<p>Todos os produtos</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>						
					</div>
					</a>

					<a href="<?php echo base_url() ?>relatorios/produtos/minimo" class="small-box-footer" target="_blank">
					<div class="small-box bg-blue">						
						<div class="inner">
							<h3><?php echo $totalEstoqueMinimo; ?></h3>

							<p>Com estoque minimo</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>						
					</div>
					</a>

					<a href="<?php echo base_url() ?>relatorios/produtos/valorestoque" class="small-box-footer" target="_blank">
					<div class="small-box bg-blue">
						
						<div class="inner">
							<h3><?php echo '$ '.number_format($totalValorEstoque[0]->totalValor,2,',','.'); ?></h3>

							<p>Valor em estoque</p>
						</div>
						<div class="icon">
							<i class="fa fa-dropbox"></i>
						</div>
						
					</div>
					</a>

				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis - Etiquetas</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<form action="<?php echo base_url()?>index.php/relatorios/produtosEtiquetaCustom" method="post" target="_blank">
							
							<div class="col-md-4">
								<div class="form-group">
									<label>Tipo</label>
									<select class="form-control" name="tipo_etiqueta">
										<option value="">Selecione</option>									
										<option value="01">Cód Barra</option>
										<option value="02">Qr-Code</option>
										<option value="03">Só Nome</option>
									</select>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label>Grupo</label>		
									<select id="produto_categoria_id" class="form-control" data-placeholder="Selecione o grupo de produtos" name="produto_categoria_id" >
					                  <option value="">Selecione</option>
					                  <?php foreach ($grupo as $g) { ?>
					                  <option value='<?php echo $g->categoria_prod_id; ?>'><?php echo $g->categoria_prod_descricao; ?> </option>
					                  <?php } ?>                       
					                </select>
								</div>
							</div>
						
							<div class="col-md-4">
								<label>&nbsp;</label>
								<button type="submit" class="btn btn-primary btn-block">
									<i class="fa fa-print"></i> Imprimir
								</button>
							</div>
						    </form>
						</div>
					</div>
				</div>
			</div>

			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Relatórios Customizáveis</h3>
				</div>
				<!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Venda de</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerVendaIncio" placeholder="dd/mm/aaaa">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>até</label>
									<div class="input-group date">
										<div class="input-group-addon">
											<i class="fa fa-calendar"></i>
										</div>
										<input type="text" class="form-control pull-right" id="datepickerVendaFim" placeholder="dd/mm/aaaa">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Exibir produtos</label>
									<select class="form-control">
										<option>Sim</option>
										<option>Não</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label>Ordenar por</label>
									<select class="form-control">
										<option>Nome cliente A - Z</option>
										<option>Nome cliente Z - A</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label>Posição Impressão</label>
									<select class="form-control">
										<option>Retrato</option>
										<option>Paisagem</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="row no-print text-center" style="margin-top: 40px">
						<div class="col-md-12">
							<button type="button" class="btn btn-default">
								<i class="fa fa-eraser"></i> Limpar
							</button>
							<button type="button" class="btn btn-primary" style="margin-right: 5px;">
								<i class="fa fa-print"></i> Imprimir
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>

