<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url(); ?>assets/dist/img/logo.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $this->session->userdata('usuario_nome'); ?> </p>
          <a href="#"><i class="fa fa-circle text-success"></i> <?php echo $this->session->userdata('usuario_perfil_nome'); ?></a>
        </div>
      </div>
      <!-- search form -->
      <form action="<?php echo base_url(); ?>sistema/pesquisa" method="post" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="pesquisa" class="form-control" placeholder="Pesquisar...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <!-- <li class="header">MAIN NAVIGATION</li> -->
        <li class="treeview">
          <a href="<?php echo base_url(); ?>sistema/dashboard">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>           
          </a>          
        </li>
        <ul class="sidebar-menu" data-widget="tree">
          <li class="treeview">
            <a href="#">
              <i class="fa fa-calendar"></i>
              <span>Agenda</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if(verificarPermissao('vAgenda')){ ?>
              <li><a href="<?php echo base_url(); ?>agenda"><i class="fa fa-circle-o"></i> Adicionar</a></li>
              <?php } ?>
              <?php if(verificarPermissao('vAgenda')){ ?>
              <li><a href="<?php echo base_url(); ?>agenda/buscar"><i class="fa fa-circle-o"></i> Buscar</a></li>
              <?php } ?>

            </ul>                     

          </li>            
        </ul>

        <ul class="sidebar-menu" data-widget="tree">
          <li class="treeview">
            <a href="#">
              <i class="fa fa-users"></i>
              <span>Pessoas</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <?php if(verificarPermissao('vCliente')){ ?>
              <li><a href="<?php echo base_url(); ?>clientes"><i class="fa fa-circle-o"></i> Cliente</a></li>
              <?php } ?>
              <?php if(verificarPermissao('vFuncionario')){ ?>
              <li><a href="<?php echo base_url(); ?>funcionarios"><i class="fa fa-circle-o"></i> Funcionário</a></li>
              <?php } ?>

            </ul>                     

          </li>            
        </ul>
        



        <ul class="sidebar-menu" data-widget="tree">
          <?php if(verificarPermissao('vFinanceiro')){ ?>
<!--           <li class="treeview">
            <a href="#">
              <i class="fa fa-money"></i>
              <span>Financeiro</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">             
              <li><a href="<?php echo base_url(); ?>financeiro"><i class="fa fa-circle-o"></i> Lançamentos</a></li>  
            </ul>  
          </li>  -->
          <?php } ?>           
        </ul>


      <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>Relatórios</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
<!--           <ul class="treeview-menu">
            <?php if(verificarPermissao('clienteRelatorio')){ ?>
            <li><a href="<?php echo base_url(); ?>relatorios/clientes"><i class="fa fa-circle-o"></i> Clientes</a></li>
            <?php } ?>     
            <?php if(verificarPermissao('fornecedorRelatorio')){ ?>
            <li><a href="<?php echo base_url(); ?>relatorios/forncedor"><i class="fa fa-circle-o"></i> Forncedor</a></li>
            <?php } ?>  
            <?php if(verificarPermissao('funcionarioRelatorio')){ ?>
            <li><a href="<?php echo base_url(); ?>relatorios/funcionarios"><i class="fa fa-circle-o"></i> Funcionário</a></li>
            <?php } ?>       
            <?php if(verificarPermissao('produtoRelatorio')){ ?>
            <li><a href="<?php echo base_url(); ?>relatorios/produtos"><i class="fa fa-circle-o"></i> Produtos</a></li>
            <?php } ?> 
          </ul>   -->               

        </li>   
        
      </ul> 

      <ul class="sidebar-menu" data-widget="tree">
        <li class="treeview">
          <a href="#">
            <i class="fa fa-gear"></i>
            <span>Configurações</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <?php if(verificarPermissao('vUsuario')){ ?>
            <li><a href="<?php echo base_url(); ?>usuarios"><i class="fa fa-circle-o"></i> Usuários</a></li>
            <?php } ?>
            <?php if(verificarPermissao('vPerfil')){ ?>
            <li><a href="<?php echo base_url(); ?>perfil"><i class="fa fa-circle-o"></i> Perfis</a></li>
            <!-- <li><a href="<?php echo base_url(); ?>parametro/editar"><i class="fa fa-circle-o"></i> Parâmetros </a></li> -->
            <?php } ?>
            <?php if(verificarPermissao('vEmitente')){ ?>
            <li><a href="<?php echo base_url(); ?>sistema/emitente"><i class="fa fa-circle-o"></i> Emitente</a></li>
            <?php } ?>
            <?php if(verificarPermissao('vBackup')){ ?>
            <li><a href="<?php echo base_url(); ?>sistema/backup"><i class="fa fa-circle-o"></i> Backup</a></li>
            <?php } ?>
          </ul>
        </li>            
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php echo ucfirst($this->uri->segment(1));?>
        <small><?php echo ucfirst($this->uri->segment(2));?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo base_url();?>sistema/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="<?php echo base_url().$this->uri->segment(1);?>"><?php echo ucfirst($this->uri->segment(1));?></a></li>
        <li class="active"><?php echo ucfirst($this->uri->segment(2));?></li>
      </ol>
    </section>

  <!-- Tratamento de mensagens do sitema -->
  <?php if($this->session->flashdata('error') or $this->session->flashdata('erro')){ ?>
      <div class="col-md-12">
        <div class="alert alert-danger alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h5><?php echo $this->session->flashdata('erro') ? $this->session->flashdata('erro') : $this->session->flashdata('error') ; ?></h5>                
        </div>
      </div>
      <?php }else if($this->session->flashdata('success')){ ?>
      <div class="col-md-12">
        <div class="alert alert-success alert-dismissible">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h5></i> <?php echo $this->session->flashdata('success'); ?></h5>               
        </div>
      </div>
      <?php } ?>