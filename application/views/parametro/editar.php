<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="" method="post">
        <input type="hidden" class="form-control" name="parametro_id" id="id" value="<?php echo $dados[0]->parametro_id; ?>" placeholder="id">
          <div class="box-body">

            <div class="form-group">
              <label for="parametro_cart_debito" class="col-sm-2 control-label">% cartão débito*</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="parametro_cart_debito" id="parametro_cart_debito" value="<?php echo $dados[0]->parametro_cart_debito; ?>" placeholder="Descrição">
              </div>
            </div> 

            <div class="form-group">
              <label for="parametro_cart_credito" class="col-sm-2 control-label">% cartão credito*</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="parametro_cart_credito" id="parametro_cart_credito" value="<?php echo $dados[0]->parametro_cart_credito; ?>" placeholder="Descrição">
              </div>
            </div>   
   

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" formaction="<?php echo current_url(); ?>" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
            <button type="submit" formaction="<?php echo base_url(); ?>parametro/updateMassa" class="btn btn-success pull-right">Update Produtos</button></a>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>
<style type="text/css">
  button{
    margin: 0 5px;
}
</style>