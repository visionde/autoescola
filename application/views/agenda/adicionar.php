<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>

      <form class="form-horizontal" action="<?php echo current_url(); ?>/adicionar" method="post">
          <div class="box-body">
              <div class="form-group">
                <label for="semana" class="col-sm-2 control-label">Semana</label>
                 <div class="col-sm-3">    
                   <input type='text' name="semana" class="form-control" id='weekpicker' placeholder="Selecione" />
                 </div>
              </div>
              <div class="form-group">
                <label for="funcionario" class="col-sm-2 control-label">Instrutor</label>
                <div class="col-sm-3">              
                  
                  <select class="form-control" name="funcionario">
                    <option value="">Selecione</option>
                    <?php foreach ($instrutor as $valor) { ?>
                      <option value='<?php echo $valor->funcionario_id; ?>'><?php echo $valor->funcionario_nome; ?> </option>
                    <?php } ?>
                  </select>
                </div>
              </div>

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?>sistema/dashboard" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right">Adicionar</button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function(){
    var startDate,
    endDate;

  $('#weekpicker').datepicker({
    autoclose: true,
    format :'dd/mm/yyyy',
    forceParse :false
    }).on("changeDate", function(e) {
    //console.log(e.date);
    var date = e.date;
    startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay());
    endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay()+6);
    //$('#weekpicker').datepicker("setDate", startDate);
    $('#weekpicker').datepicker('update', startDate);
    $('#weekpicker').val(startDate.getDate() + '/' + (startDate.getMonth() + 1) + '/' +  startDate.getFullYear() + ' - ' + endDate.getDate()  + '/' + (endDate.getMonth() + 1) + '/' +  endDate.getFullYear());
   })
});
</script>
