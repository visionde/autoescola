<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>

      <form class="form-horizontal" action="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>/editarExe" method="post">
          <div class="box-body">
            <input type="hidden" name="id_agenda" value="<?php echo $this->uri->segment(3); ?>">
            <input type="hidden" name="contador" value="<?php echo $dados[0]['agenda_idItens']?>">">

           <table  class="table table-bordered table-striped">
                <thead>
                  <tr>
                      <th class="columns01 col_default">Data</th>
                      <th class="columns02 col_default"><?php echo $d1 ?></th>
                      <th class="columns02 col_default"><?php echo $d2 ?></th>
                      <th class="columns02 col_default"><?php echo $d3 ?></th>
                      <th class="columns02 col_default"><?php echo $d4 ?></th>
                      <th class="columns02 col_default"><?php echo $d5 ?></th>
                      <th class="columns02 col_default"><?php echo $d6 ?></th>      
                  </tr>
                  <tr>
                      <th class="columns01 col_default">Hora</th>
                      <th class="columns02 col_default">Segunda</th>
                      <th class="columns02 col_default">Terça</th>
                      <th class="columns02 col_default">Quarta</th>
                      <th class="columns02 col_default">Quinta</th>
                      <th class="columns02 col_default">Sexta</th>
                      <th class="columns02 col_default">Sabado</th>        
                  </tr>
                  <tr>
                      <th class="columns01 col_default">07:00</th>
                      <td><input type="text" name="1_1" value="<?php echo $dados[0]['01']?>"></td>
                      <td><input type="text" name="2_1" value="<?php echo $dados[1]['01']?>"></td>
                      <td><input type="text" name="3_1" value="<?php echo $dados[2]['01']?>"></td>
                      <td><input type="text" name="4_1" value="<?php echo $dados[3]['01']?>"></td>
                      <td><input type="text" name="5_1" value="<?php echo $dados[4]['01']?>"></td>
                      <td><input type="text" name="6_1" value="<?php echo $dados[5]['01']?>"></td>
                  </tr>
                  <tr>
                      <th class="columns01 col_default">07:50</th>
                      <td><input type="text" name="1_2" value="<?php echo $dados[0]['02']?>"></td>
                      <td><input type="text" name="2_2" value="<?php echo $dados[1]['02']?>"></td>
                      <td><input type="text" name="3_2" value="<?php echo $dados[2]['02']?>"></td>
                      <td><input type="text" name="4_2" value="<?php echo $dados[3]['02']?>"></td>
                      <td><input type="text" name="5_2" value="<?php echo $dados[4]['02']?>"></td>
                      <td><input type="text" name="6_2" value="<?php echo $dados[5]['02']?>"></td>
                  </tr>
                  <tr>
                      <th class="columns01 col_default">08:40</th>
                      <td><input type="text" name="1_3" value="<?php echo $dados[0]['03']?>"></td>
                      <td><input type="text" name="2_3" value="<?php echo $dados[1]['03']?>"></td>
                      <td><input type="text" name="3_3" value="<?php echo $dados[2]['03']?>"></td>
                      <td><input type="text" name="4_3" value="<?php echo $dados[3]['03']?>"></td>
                      <td><input type="text" name="5_3" value="<?php echo $dados[4]['03']?>"></td>
                      <td><input type="text" name="6_3" value="<?php echo $dados[5]['03']?>"></td>
                  </tr>
                  <tr>
                      <th class="columns01 col_default">09:30</th>
                      <td><input type="text" name="1_4" value="<?php echo $dados[0]['04']?>"></td>
                      <td><input type="text" name="2_4" value="<?php echo $dados[1]['04']?>"></td>
                      <td><input type="text" name="3_4" value="<?php echo $dados[2]['04']?>"></td>
                      <td><input type="text" name="4_4" value="<?php echo $dados[3]['04']?>"></td>
                      <td><input type="text" name="5_4" value="<?php echo $dados[4]['04']?>"></td>
                      <td><input type="text" name="6_4" value="<?php echo $dados[5]['04']?>"></td>
                  </tr>
                  <tr>
                      <th class="columns01 col_default">10:20</th>
                      <td><input type="text" name="1_5" value="<?php echo $dados[0]['05']?>"></td>
                      <td><input type="text" name="2_5" value="<?php echo $dados[1]['05']?>"></td>
                      <td><input type="text" name="3_5" value="<?php echo $dados[2]['05']?>"></td>
                      <td><input type="text" name="4_5" value="<?php echo $dados[3]['05']?>"></td>
                      <td><input type="text" name="5_5" value="<?php echo $dados[4]['05']?>"></td>
                      <td><input type="text" name="6_5" value="<?php echo $dados[5]['05']?>"></td>
                  </tr>
                  <tr>
                      <th class="columns01 col_default">11:10</th>
                      <td><input type="text" name="1_6" value="<?php echo $dados[0]['06']?>"></td>
                      <td><input type="text" name="2_6" value="<?php echo $dados[1]['06']?>"></td>
                      <td><input type="text" name="3_6" value="<?php echo $dados[2]['06']?>"></td>
                      <td><input type="text" name="4_6" value="<?php echo $dados[3]['06']?>"></td>
                      <td><input type="text" name="5_6" value="<?php echo $dados[4]['06']?>"></td>
                      <td><input type="text" name="6_6" value="<?php echo $dados[5]['06']?>"></td>
                  </tr>
                  <tr>
                      <th class="columns01 col_default">Almoço</th>
                      <th class="columns01 col_default"><center>Almoço</center></th>
                      <th class="columns01 col_default"><center>Almoço</center></th>
                      <th class="columns01 col_default"><center>Almoço</center></th>
                      <th class="columns01 col_default"><center>Almoço</center></th>
                      <th class="columns01 col_default"><center>Almoço</center></th>
                  </tr>
                  <tr>
                      <th class="columns01 col_default">13:00</th>
                      <td><input type="text" name="1_7" value="<?php echo $dados[0]['07']?>"></td>
                      <td><input type="text" name="2_7" value="<?php echo $dados[1]['07']?>"></td>
                      <td><input type="text" name="3_7" value="<?php echo $dados[2]['07']?>"></td>
                      <td><input type="text" name="4_7" value="<?php echo $dados[3]['07']?>"></td>
                      <td><input type="text" name="5_7" value="<?php echo $dados[4]['07']?>"></td>
                  </tr>
                  <tr>
                      <th class="columns01 col_default">14:00</th>
                      <td><input type="text" name="1_8" value="<?php echo $dados[0]['08']?>"></td>
                      <td><input type="text" name="2_8" value="<?php echo $dados[1]['08']?>"></td>
                      <td><input type="text" name="3_8" value="<?php echo $dados[2]['08']?>"></td>
                      <td><input type="text" name="4_8" value="<?php echo $dados[3]['08']?>"></td>
                      <td><input type="text" name="5_8" value="<?php echo $dados[4]['08']?>"></td>
                  </tr>
                  <tr>
                      <th class="columns01 col_default">15:00</th>
                      <td><input type="text" name="1_9" value="<?php echo $dados[0]['09']?>"></td>
                      <td><input type="text" name="2_9" value="<?php echo $dados[1]['09']?>"></td>
                      <td><input type="text" name="3_9" value="<?php echo $dados[2]['09']?>"></td>
                      <td><input type="text" name="4_9" value="<?php echo $dados[3]['09']?>"></td>
                      <td><input type="text" name="5_9" value="<?php echo $dados[4]['09']?>"></td>
                  </tr>
                  <tr>
                      <th class="columns01 col_default">16:00</th>
                      <td><input type="text" name="1_10" value="<?php echo $dados[0]['10']?>"></td>
                      <td><input type="text" name="2_10" value="<?php echo $dados[1]['10']?>"></td>
                      <td><input type="text" name="3_10" value="<?php echo $dados[2]['10']?>"></td>
                      <td><input type="text" name="4_10" value="<?php echo $dados[3]['10']?>"></td>
                      <td><input type="text" name="5_10" value="<?php echo $dados[4]['10']?>"></td>
                  </tr>
                  <tr>
                      <th class="columns01 col_default">17:00</th>
                      <td><input type="text" name="1_11" value="<?php echo $dados[0]['11']?>"></td>
                      <td><input type="text" name="2_11" value="<?php echo $dados[1]['11']?>"></td>
                      <td><input type="text" name="3_11" value="<?php echo $dados[2]['11']?>"></td>
                      <td><input type="text" name="4_11" value="<?php echo $dados[3]['11']?>"></td>
                      <td><input type="text" name="5_11" value="<?php echo $dados[4]['11']?>"></td>
                  </tr>
                  <tr>
                      <th class="columns01 col_default">18:00</th>
                      <td><input type="text" name="1_12" value="<?php echo $dados[0]['12']?>"></td>
                      <td><input type="text" name="2_12" value="<?php echo $dados[1]['12']?>"></td>
                      <td><input type="text" name="3_12" value="<?php echo $dados[2]['12']?>"></td>
                      <td><input type="text" name="4_12" value="<?php echo $dados[3]['12']?>"></td>
                      <td><input type="text" name="5_12" value="<?php echo $dados[4]['12']?>"></td>
                  </tr>

                  </thead>

              </table>

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?>/agenda" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right">Confirmar</button>
          </div>
          <!-- /.box-footer -->
        </form>

      </div>
    </div>
  </div>
</section>