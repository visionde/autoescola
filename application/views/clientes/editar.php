<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title"></h3>
      </div>
        <!-- /.box-header -->
        <!-- form start -->
        <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post">
          <input type="hidden" class="form-control" name="id" id="id"  value="<?php echo $dados[0]->cliente_id; ?>">
          <div class="box-body">

          <div class="form-group">
              <label for="loja" class="col-sm-2 control-label">Lojas </label>
              <div class="col-sm-5">              
                <select id="lojas" class="form-control select2" multiple="multiple" data-placeholder="Selecione as lojas" name="lojas[]" disabled="disabled">
                  <?php 
                  foreach ($lojas as $s) {
                    echo "<option value='".strtoupper($s['lojas'])."' selected>".strtoupper($s['lojas'])."</option>";
                  }
                  ?>                        
                </select>
              </div>
            </div> 

            <div class="form-group">
              <label for="cliente_tipo" class="col-sm-2 control-label">Tipo</label>
              <div class="col-sm-5">
                            
                <select required class="form-control" name="cliente_tipo" id="tipo">
                    <option value='1' <?php echo ($dados[0]->cliente_tipo == 1)?'Selected':'';?>>Pessoa Física</option>
                    <option value='2' <?php echo ($dados[0]->cliente_tipo == 2)?'Selected':'';?>>Pessoa Jurídica</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_cpf_cnpj" class="col-sm-2 control-label">CPF</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="cliente_cpf_cnpj" id="cliente_cpf_cnpj"  value="<?php echo $dados[0]->cliente_cpf_cnpj; ?>" placeholder="CPF/CNPJ">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_nome" class="col-sm-2 control-label">Nome</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="cliente_nome" name="cliente_nome"  value="<?php echo $dados[0]->cliente_nome; ?>" placeholder="Nome Completo">
              </div>
            </div> 

            <div class="form-group">
              <label for="cliente_cep" class="col-sm-2 control-label">CEP</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="cep" name="cliente_cep"  value="<?php echo $dados[0]->cliente_cep; ?>" placeholder="Cep">
              </div>
            </div>  

            <div class="form-group">
              <label for="cliente_endereco" class="col-sm-2 control-label">Endereço</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="endereco" name="cliente_endereco"  value="<?php echo $dados[0]->cliente_endereco; ?>" placeholder="Endereço">
              </div>
            </div> 
            
            <div class="form-group">
              <label for="cliente_bairro" class="col-sm-2 control-label">Bairro</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="bairro" name="cliente_bairro"  value="<?php echo $dados[0]->cliente_bairro; ?>" placeholder="Bairro">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_estado" class="col-sm-2 control-label">Estado</label>
              <div class="col-sm-5">              
                
                <select class="form-control" name="cliente_estado" id="estado">
                  <option value="">Selecione</option>
                  <?php foreach ($estados as $valor) { ?>
                  <?php $selected = ($valor->sigla == $dados[0]->cliente_estado)?'SELECTED':''; ?>
                    <option value='<?php echo $valor->sigla; ?>' <?php echo $selected; ?>><?php echo $valor->nome; ?> </option>
                  <?php } ?>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_cidade" class="col-sm-2 control-label">Cidade</label>
              <div class="col-sm-5">              
                <select class="form-control" name="cliente_cidade" id="cidade">
                  <option value="">Escolha um estado</option>    
                  <?php foreach ($cidades as $valor) { ?>
                  <?php $selected = ($valor->nome == strtoupper($dados[0]->cliente_cidade))?'SELECTED':''; ?>
                    <option value='<?php echo $valor->nome; ?>'  <?php echo $selected; ?>><?php echo $valor->nome; ?> </option>
                  <?php } ?>                                  
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_numero" class="col-sm-2 control-label">Número</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="numero" name="cliente_numero"  value="<?php echo $dados[0]->cliente_numero; ?>" placeholder="Número">
              </div>
            </div>       

            <div class="form-group">
              <label for="cliente_complemento" class="col-sm-2 control-label">Complemento</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="completo" name="cliente_complemento"  value="<?php echo $dados[0]->cliente_complemento; ?>" placeholder="Complemento">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_telefone" class="col-sm-2 control-label">Telefone</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" id="telefone"  name="cliente_telefone"  value="<?php echo $dados[0]->cliente_telefone; ?>" placeholder="Telefone">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_celular" class="col-sm-2 control-label">Celular</label>
              <div class="col-sm-5">
                <input type="text" class="form-control" id="celular" name="cliente_celular"  value="<?php echo $dados[0]->cliente_celular; ?>" placeholder="Celular">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_email" class="col-sm-2 control-label">E-mail</label>
              <div class="col-sm-5">                
                <input type="text" class="form-control" id="e-mail" name="cliente_email"  value="<?php echo $dados[0]->cliente_email; ?>" placeholder="E-mail">
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_tipo" class="col-sm-2 control-label">Situação</label>
              <div class="col-sm-5">                            
                <select required class="form-control" name="cliente_ativo" id="cliente_ativo">
                    <option value='1' <?php echo ($dados[0]->cliente_ativo == 1)?'Selected':'';?>>Ativo</option>
                    <option value='0' <?php echo ($dados[0]->cliente_ativo == 0)?'Selected':'';?>>Inativo</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <label for="cliente_limite_cred_cliente" class="col-sm-2 control-label">R$ limite credito *</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="cliente_limite_cred_cliente" id="cliente_limite_cred_cliente" value="<?php echo $dados[0]->cliente_limite_cred_cliente; ?>" placeholder="limite crédito">
              </div>
            </div> 

            <div class="form-group">
              <label for="cliente_limite_dias_cliente" class="col-sm-2 control-label">limite de dias *</label>
              <div class="col-sm-5">              
                <input type="text" class="form-control" name="cliente_limite_dias_cliente" id="cliente_limite_dias_cliente" value="<?php echo $dados[0]->cliente_limite_dias_cliente; ?>" placeholder="dias limite crédito">
              </div>
            </div> 

          </div>
          <!-- /.box-body -->
          <div class="box-footer">
            <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>
            <button type="submit" class="btn btn-primary pull-right"><?php echo ucfirst($this->uri->segment(2)); ?></button>
          </div>
          <!-- /.box-footer -->
        </form>
      </div>
    </div>
  </div>
</section>