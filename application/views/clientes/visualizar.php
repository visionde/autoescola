<section class="content">
  <div class="row">
    <div class="col-md-12">
    <!-- Horizontal Form -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <!-- /.box-header -->
        
        <div class="col-md-12">
          <!-- Custom Tabs (Pulled to the right) -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
              <li><a href="#tab_1" data-toggle="tab">localização</a></li>
              <li><a href="#tab_2" data-toggle="tab">vendas</a></li>
              <li class="active"><a href="#tab_3" data-toggle="tab">Dados</a></li>      
              <li class="pull-left header"><i class="fa fa-th"></i> Historico Cliente</li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane" id="tab_1">
                <b>Localização do cliente pelo google Maps.</b>

                
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <b>Lista de todas as vendas comprada pelo cliente com sinalizaçao (Pago ou Pendete).</b>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane active" id="tab_3">
                  <div class="row">
                        <div class="col-md-6">
                          <label for="cliente_tipo">Tipo</label>                        
                          <select disabled required class="form-control" name="cliente_tipo" id="tipo">
                              <option value='1' <?php echo ($dados[0]->cliente_tipo == 1)?'Selected':'';?>>Pessoa Física</option>
                              <option value='2' <?php echo ($dados[0]->cliente_tipo == 2)?'Selected':'';?>>Pessoa Jurídica</option>
                          </select>
                        </div>

                        <div class="col-md-6 form-group">
                          <label for="descricao">CEP</label>
                         <input disabled type="text" class="form-control" id="cep" name="cliente_cep"  value="<?php echo $dados[0]->cliente_cep; ?>" placeholder="Cep">

                        </div>

                        <div class="col-md-6">
                          <label for="descricao">CPF/CNPJ</label>
                          <input disabled type="text" class="form-control" name="cliente_cpf_cnpj" id="cliente_cpf_cnpj"  value="<?php echo $dados[0]->cliente_cpf_cnpj; ?>" placeholder="CPF/CNPJ">
                           
                        </div>

                        <div class="col-md-6 form-group">
                          <label for="descricao">Endereço</label>
                          <input disabled type="text" class="form-control" id="endereco" name="cliente_endereco"  value="<?php echo $dados[0]->cliente_endereco; ?>" placeholder="Endereço">
                        </div>
                         
                        <div class="col-md-6">
                          <label for="descricao">Nome</label>
                          <input disabled type="text" class="form-control" id="cliente_nome" name="cliente_nome"  value="<?php echo $dados[0]->cliente_nome; ?>" placeholder="Nome Completo">
                        </div>

                        <div class="col-md-6 form-group">
                          <label for="descricao">Bairro</label>
                          <input disabled type="text" class="form-control" id="bairro" name="cliente_bairro"  value="<?php echo $dados[0]->cliente_bairro; ?>" placeholder="Bairro">
                        </div>

                          <div class="col-md-6">
                            <label for="descricao">Telefone</label>
                            <input disabled type="text" class="form-control" id="telefone"  name="cliente_telefone"  value="<?php echo $dados[0]->cliente_telefone; ?>" placeholder="Telefone">
                          </div>

                          <div class="col-md-6 form-group">
                            <label for="descricao">Número</label>
                              <input disabled type="text" class="form-control" id="numero" name="cliente_numero"  value="<?php echo $dados[0]->cliente_numero; ?>" placeholder="Número">                            
                          </div>

                          <div class="col-md-6">
                            <label for="descricao">Celular</label>
                             <input disabled type="text" class="form-control" id="celular" name="cliente_celular"  value="<?php echo $dados[0]->cliente_celular; ?>" placeholder="Celular">
                          </div>

                          <div class="col-md-6">
                            <label for="descricao">Complemento</label>
                              <input disabled type="text" class="form-control" id="completo" name="cliente_complemento"  value="<?php echo $dados[0]->cliente_complemento; ?>" placeholder="Complemento">
                          </div>   

                          <div class="col-md-6">
                            <label for="descricao">E-mail</label>
                            <input disabled type="text" class="form-control" id="e-mail" name="cliente_email"  value="<?php echo $dados[0]->cliente_email; ?>" placeholder="E-mail">
                          </div>

                          <div class="col-md-6">
                            <label for="descricao">Cidade</label>
                              <select disabled class="form-control" name="cliente_cidade" id="cidade">
                                <option value="">Selecione</option>
                                <?php foreach ($cidades as $valor) { ?>
                                <?php $selected = ($valor->nome == strtoupper($dados[0]->cliente_cidade))?'SELECTED':''; ?>
                                  <option value='<?php echo $valor->nome; ?>'  <?php echo $selected; ?>><?php echo $valor->nome; ?> </option>
                                <?php } ?>                                    
                              </select>
                          </div>  

                          <div class="col-md-6">
                            <label for="cliente_limite_cred_cliente">Limite crédito</label>
                            <input disabled type="text" class="form-control" id="cliente_limite_cred_cliente" name="cliente_limite_cred_cliente"  value="<?php echo 'R$ '. $dados[0]->cliente_limite_cred_cliente; ?>" placeholder="Limite Crédito">
                          </div>

                          <div class="col-md-6">
                            <label for="descricao">Estado</label>
                              <select disabled class="form-control" name="cliente_estado" id="estado">
                                <option value="">Selecione</option>
                                <?php foreach ($estados as $valor) { ?>
                                <?php $selected = ($valor->sigla == $dados[0]->cliente_estado)?'SELECTED':''; ?>
                                  <option value='<?php echo $valor->sigla; ?>' <?php echo $selected; ?>><?php echo $valor->nome; ?> </option>
                                <?php } ?>
                              </select>
                          </div>               

                    </div>
             

              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->



        <!-- /.box-body -->
        <div class="box-footer">
          <a href="<?php echo base_url(); ?><?php echo $this->uri->segment(1);?>" class="btn btn-default">Voltar</a>          
        </div>
        <!-- /.box-footer -->        
      </div>
    </div>
  </div>
</section>