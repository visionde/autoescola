$('#btnAdicionarProduto').click(function(){

  var id = $('#movimentacao_produto_id').val();
  tipo = $('#tipomovimento').val();
  quantidade = $('#quantidade').val();
  precoUnitario = $('#precoUnitario').val();
  produto  = $('#produto_id').val();

 // alert(produto);

 $.ajax({
   type: "POST",
   url: base_url+"movimentacao/adicionarProduto",
   data: { movimentacao_produto_id: id,
     tipo: tipo,
     quantidade: quantidade,
     precoUnitario: precoUnitario,
     produto_id: produto },
     dataType: 'json',
     success: function(data)
     {
       if(data.result == true){
         $("#divProdutos" ).load("<?php echo current_url();?> #divProdutos" );
         $("#quantidade").val('');
         $("#precoUnitario").val('');
         $("#produto").val('').focus();
       }
       else{
         alert('Ocorreu um erro ao tentar adicionar produto.');
       }
     }
   });

});

//Pedido x Venda

$('#btnAdicionarProdutoPedido').click(function(){

  var id         = $('#idPedidos').val();
      quantidade = $('#quantidade').val();
      subTotal   = $('#subTotal').val();
      produto    = $('#produto_desc').val();

     $.ajax({
          type: "POST",
          url: base_url+"pedido/adicionarProduto",
          data: { idPedidos: id,
                  quantidade: quantidade,
                  subTotal: subTotal,
                  produto_id: produto },
          dataType: 'json',
          success: function(data)
          {
            if(data.result == true){
                location.reload('#divProdutos');
                // $("#divProdutos" ).load("<?php echo current_url();?> #divProdutos" );
                // $("#quantidade").val('');
                // // $("#precoUnitario").val('');
                // $("#produto").val('').focus();
            }
            else{
                alert('Ocorreu um erro ao tentar adicionar produto. Tente Novamente!');
            }
          }
          });


});


//Pedido x Compra - Fornecedor

$('#btnAdicionarProdutoPedidoFornecedor').click(function(){

  var id         = $('#idPedidos').val();
      quantidade = $('#quantidade').val();
     // subTotal   = $('#subTotal').val();
      produto    = $('#produto_desc').val();

     $.ajax({
          type: "POST",
          url: base_url+"pedidofornecedor/adicionarProduto",
          data: { idPedidos: id,
                  quantidade: quantidade,
                 // subTotal: subTotal,
                  produto_id: produto },
          dataType: 'json',
          success: function(data)
          {
            if(data.result == true){
                location.reload('#divProdutos');
                // $("#divProdutos" ).load("<?php echo current_url();?> #divProdutos" );
                // $("#quantidade").val('');
                // // $("#precoUnitario").val('');
                // $("#produto").val('').focus();
            }
            else{
                alert('Ocorreu um erro ao tentar adicionar produto. Tente Novamente!');
            }
          }
          });


});


$('#btnVendaPedido').click(function(){

  var id         = $('#idPedidos').val();
      total      = $('#total').val();
      cliente    = $('#cliente_id').val();


     $.ajax({
          type: "POST",
          url: base_url+"pedido/VendaPedido",
          data: { idPedidos: id,
                  total: total,
                  cliente: cliente,
                },
          dataType: 'json',
          success: function(data)
          {
            if(data.result == true){

               window.location.replace(base_url+"/pedido")
                // location.reload('#divProdutos');
                // $("#divProdutos" ).load("<?php echo current_url();?> #divProdutos" );
                // $("#quantidade").val('');
                // // $("#precoUnitario").val('');
                // $("#produto").val('').focus();
            }
            else{
                alert('Os itens marcardos de vermelho não pode ser transformado em venda!.');
            }
          }
          });


});



$('input').bind('keypress', function(e) {
  if(e.keyCode == 13) {
    return false;
  }
});


//Vai para geral
$('#example').DataTable();
$('#example').parent().addClass('table-responsive');


function excluir(url) {
  console.log(url)
  $('#excluirId').attr('href', url);
}

//vendas

function excluirVendas(idVendas, quantidade, produto, produto_id, idItens) {
  // console.log(idVendas)
  // $('#exVendas').attr('href', url);
  $("#btn").text(quantidade);
  document.getElementById('idVendas').value = idVendas
  document.getElementById('quantidadeH').value = quantidade
  document.getElementById('produtoM').value = produto
  document.getElementById('produtoID').value = produto_id
  document.getElementById('idItens').value = idItens

}

 $("#quantidadeM").on("keyup", function(){
     
     var quantidadeH = parseInt($("#quantidadeH").val());
     var quantidadeM = parseInt($("#quantidadeM").val());

     console.log(quantidadeM)
     
     if((quantidadeH < quantidadeM)){
        $("#quantidadeM").val('').blur().focus();
     }
    
     
  });


    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass   : 'iradio_flat-blue'
    });


    $('#tipo_moeda').change(function(event) {
      var tipo_moeda = $(this).val();
      $('#div_cotacao_moeda').hide();

      if(tipo_moeda == 'real'){      
        $('#cotacao_moeda').mask('#.##0,00', {reverse: true});
        $('#div_percent_real').show();
        $('#div_cotacao_moeda').hide();
      }else{
        $('#div_cotacao_moeda').show();
        $('#div_percent_real').hide();
        // $('#cotacao_moeda').inputmask('99', {reverse: true});
      }
    });

    $('#movimentacao_produto_tipo').change(function(){
      let tipo = $(this).val();

      if (tipo == 'Entrada'){
        $('#saida').hide();
        $('#div_tipo_moeda').show();
        $('#fornecedor').show();
      } else {
        $('#saida').show();
        $('#div_tipo_moeda').hide();
      }

    });

    $('#tipo_saida').change(function(){
        let tipoSaida = $(this).val();
        if (tipoSaida == 'fornecedor'){
          $('#fornecedor').show();
          $('#div_filial').hide();
        } else {
          $('#fornecedor').hide();
          $('#div_filial').show();
        }
    });
    $('#tipo').change(function(event) {
      var tipo = $(this).val();

      if(tipo == 1){      
        $('#cpfCnpj').inputmask('999.999.999-99');
      }else{
        $('#cpfCnpj').inputmask('99.999.999/9999-99');
      }
    });

    $('#produto_categoria_id').change(function(event) {
      var str = $('#produto_categoria_id').find(":selected").text();  
      var res = str.substr(0, 2);
      $('#produto_codigo').val(res);
      console.log($('#produto_codigo').val())
    });

    $('#produto_categoria_id').change(function(){
   
         var grupo = $('#produto_categoria_id').val();
         $.ajax({
          method: "POST",
          url: base_url+"produto/selecionarCategoria/",
          dataType: "html",
          data: { grupo: grupo}
          })
          .done(function( response ) {

          $('#margem_cat').html(response);
          $('#margem_cat').removeAttr("disabled", "disabled" );
          
          });
         
      });

    $('#produto_preco_custo').blur(function(){
 
       var margem = $('#margem_categoria').val();
       var custo  = parseFloat($('#produto_preco_custo').val()).toFixed(2);
       var cal    = parseFloat((custo/100)) * parseFloat(margem);
       var minimo = parseFloat(custo) + parseFloat(cal);

       $('#produto_preco_minimo_venda').val(minimo.toFixed(2));
       
    });


    $('#produto_preco_venda').blur(function(){

       var debito = $('#cartao_debito').val();
       var credito = $('#cartao_credto').val();
       var custo  = parseFloat($('#produto_preco_venda').val()).toFixed(2);

       var cal_debito   = parseFloat((custo/100)) * parseFloat(debito);
       var valor_debito = parseFloat(custo) + parseFloat(cal_debito);

       var cal_credito   = parseFloat((custo/100)) * parseFloat(credito);
       var valor_credito = parseFloat(custo) + parseFloat(cal_credito);

       $('#produto_preco_cart_debito').val(valor_debito.toFixed(2));
       $('#produto_preco_cart_credito').val(valor_credito.toFixed(2));
            
    });


    $('#cep').blur(function(event) {

      var cep = $(this).val();

      $.ajax({
        url: base_url+'/sistema/wscep/',
        type: 'POST',
        dataType: 'json',
        data: {cep: cep}
      })
      .always(function(response) {

        console.log(response);

        $('#carregandoCep').css('display', 'none');

        if(response.localidade !== undefined){
          $('#endereco').val(response.logradouro);
          $('#cidade').html('<option value="'+response.localidade+'">'+response.localidade+'</option>');
          $('#bairro').val(response.bairro);
          $('#estado').val(response.uf);
        }else{
          alert('CEP não encontrado!');
        }
      });

    });

    $('#cpfCnpj').blur(function(event) {

      var documento = $(this).val();

      if(documento.length == 18){

        $.ajax({
           url: base_url+'clientes/wscnpj/', //'<?php echo base_url();?>empresa/wscnpj/',
           type: 'POST',
           dataType: 'json',
           data: {documento: documento},
           beforeSend: function() {
             $('#carregando').css('display', 'block');
           }

         })
        .always(function(result) {

          $('#carregando').css('display', 'none');

          if(result.status == 'OK'){

            var cep = result.cep.replace('.','');
            var telefone = result.telefone.replace(' ','');

            // console.log(result);
            $('#nome').val(result.nome);
            $('#telefone').val(telefone);
            $('#email').val(result.email);
            $('#cep').val(cep);
            $('#endereco').val(result.logradouro);
            $('#numero').val(result.numero);
            $('#bairro').val(result.bairro);
            $('#estado').val(result.uf);
             //$('#cidade').val(result.municipio);
             $('#cidade').html('<option value="'+result.municipio+'">'+result.municipio+'</option>');


           }else{
            //alert(result.message);
          }

        });

      }

    });


 $('#estado').change(function(){

      var estado = $('#estado').val();
      $.ajax({
        method: "POST",
        url: base_url+"sistema/selecionarCidade/",
        dataType: "html",
        data: { estado: estado}
      })
      .done(function( response ) {
        console.log(response);
        $('#cidade').html(response);
        $('#cidade').removeAttr("disabled", "disabled" );

      });

    });

$('#grupo_prod').change(function(){
   
   var grupo = $('#grupo_prod').val();
   $.ajax({
    method: "POST",
    url: base_url+"movimentacao/selecionarProduto/",
    dataType: "html",
    data: { grupo: grupo}
    })
    .done(function( response ) {
      console.log('response');
    $('#produto_desc').html(response);
    $('#produto_desc').removeAttr("disabled", "disabled" );    
    
      });

    });

//Pedido

$('#grupo_prod_pedido').change(function(){
   
   var grupo = $('#grupo_prod_pedido').val();
   $.ajax({
    method: "POST",
    url: base_url+"pedido/selecionarProduto/",
    dataType: "html",
    data: { grupo: grupo}
    })
    .done(function( response ) {

    $('#produto_desc').html(response);
    $('#produto_desc').removeAttr("disabled", "disabled" );
    
    });
   
});

$('#produto_desc').change(function(){
   
   var produto = $('#produto_desc').val();
   $.ajax({
    method: "POST",
    url: base_url+"pedido/selecionarProdutoID/",
    dataType: "html",
    data: { produto: produto}
    })
    .done(function( response ) {
      console.log(response);
    $('#valord').html(response);
    $('#valord').removeAttr("disabled", "disabled" );    
    
    });
   
});

$('#produto_desc_mov').change(function(){
   
   var produto = $('#produto_desc_mov').val();

   $.ajax({
    method: "POST",
    url: base_url+"movimentacao/produtoProId/",
    dataType: "json",
    data: { produto: produto}
    })
    .done(function( resp ) {

    $('#dolar').val(resp.produto_preco_dolar);
    $('#custo').val(resp.produto_preco_custo);
    $('#minimo').val(resp.produto_preco_minimo_venda);
    $('#venda').val(resp.produto_preco_venda);

    $('#produto_id').val(resp.produto_id);
    $('#item_produto_codigo').val(resp.produto_codigo);
    $('#margem').val(resp.categoria_prod_margem);
    $('#estoque').val(resp.produto_estoque);

    $('#quantidade_mov').val('1');

    calcularTotalItem();

    $('#quantidade_mov').focus();

    });
   
});

$('#quantidade_mov').blur(function(){
  calcularTotalItem();
});

$('#addProdutoAjax').click(function(){
  let descricaoProduto = $('#modal_descricao_produto').val();
  let codigoProduto = $('#produto_codigo').val();
  let categoriaProduto = $('#produto_categoria_id').val();

  $.ajax({
    url: `${base_url}movimentacao/addProdutoAjax`,
    type: 'POST',
    datatype: 'JSON',
    data: {
      produto_descricao : descricaoProduto,
      produto_categoria_id : categoriaProduto,
      produto_codigo : codigoProduto
    }
  }).done(function(resp){
    
    if (resp) {
      console.log('Produto Adicionado com Sucesso !!!');
      $('#modal-default').modal('hide');
    } else {
      console.log('Falha ao adicionar o produto');
    }

  });
});

$('#btnEntradaProduto').click(function () {
  AddTableRow('entrada');
});

$('#btnSaidaProduto').click(function () {
  AddTableRow('saida');
});


AddTableRow = function(tipo) {

  let idProduto     = $('#produto_id').val();
  let codigoProduto = $('#item_produto_codigo').val();

  let descricao = $('#select2-produto_desc_mov-container').attr('title');
  let quantidade = parseInt($('#quantidade_mov').val());
  let dolar = parseFloat($('#dolar').val()).toFixed(2);
  let custo = parseFloat($('#custo').val()).toFixed(2);
  let minimo = parseFloat($('#minimo').val()).toFixed(2);
  let venda = parseFloat($('#venda').val()).toFixed(2);
  let totalItem = parseFloat($('#tot_item').val()).toFixed(2);

  let valorDocumento = parseFloat($('#vl_documento').val()).toFixed(2);

  let numItens = parseInt($("#itens").val());
  let volume = parseInt($("#volume").val());
  let estoque = $("#estoque").val();

  if (quantidade >= 1){

    let movimentacaoId = $("#movId").val();

    $.ajax({
      url: `${base_url}movimentacao/addItemMovimentoProduto/${movimentacaoId}`,
      type: 'POST',
      data: {idProduto, quantidade, venda, minimo, custo, dolar},
      dataType: 'JSON'
    }).done(function (resp) {

      if(resp){

          var newRow = $("<tr>");
          var cols = "";

          // cols += `<td class="col-md-1"> <input type="text" class="text-center"  value="${idProduto}" style="border: 0;" readonly></td>`;
          cols += `<td> <input type="text" class="text-center col-md-12"  value="${codigoProduto}" style="border: 0;" readonly></td>`;
          cols += `<td> <input type="text" class="text-center col-md-12"  value="${descricao}" style="border: 0;" readonly></td>`;
		
		  if (tipo == 'saida') cols += `<td class="text-center"> <span class="label label-${(estoque < quantidade ? 'danger' : 'success')}">${estoque}</span></td>`;
         
          cols += `<td> <input type="text" class="text-center col-md-12"  value="${quantidade}" style="border: 0;" readonly></td>`;
          
          if (tipo != 'saida'){
              cols += `<td> <input type="text" class="text-center col-md-12"  value="${dolar}" style="border: 0;" readonly></td>`;
              cols += `<td> <input type="text" class="text-center col-md-12"  value="${custo}" style="border: 0;" readonly></td>`;
              cols += `<td> <input type="text" class="text-center col-md-12"  value="${minimo}" style="border: 0;" readonly></td>`;
              cols += `<td> <input type="text" class="text-center col-md-12"  value="${venda}" style="border: 0;" readonly></td>`;
              cols += `<td> <input type="text" class="text-center col-md-12"  value="${totalItem}"style="border: 0;" readonly></td>`;
          }else{
              cols += `<td> <input type="text" class="text-center col-md-12"  value="${custo}" style="border: 0;" readonly></td>`;
              cols += `<td> <input type="text" class="text-center col-md-12"  value="${(custo * quantidade).toFixed(2)}" style="border: 0;" readonly></td>`;
          }

          cols += `<td>`;
          cols += `<a type="button" onclick="RemoveTableRow(this)" produtoId="${resp}" quantidade="${quantidade}" valor="${totalItem}"><i class="btn btn-danger fa fa-trash"></i></a>`;
          cols += `</td>`;

          newRow.append(cols);

          $("#tableProdutos tbody").append(newRow);
          alert(valorDocumento +'---'+ totalItem)
          $('#vl_documento').val((parseFloat(valorDocumento) + parseFloat(totalItem)).toFixed(2));
          $("#volume").val((volume + quantidade));
          $("#itens").val(((numItens + 1)));
      }
    });

  }

};

function RemoveTableRow (handler) {

  let quantidade = parseInt($(handler).attr('quantidade'));
  let produtoId = parseInt($(handler).attr('produtoId'));

  console.log(produtoId)

  let valor = parseFloat($(handler).attr('valor')).toFixed(2);
  let valorDocumento = parseFloat($('#vl_documento').val());

  let volume = parseInt($("#volume").val());

  $.ajax({
    url: `${base_url}movimentacao/deleteItemMovimentoProduto/${produtoId}`,
    type: 'POST',
    dataType: 'JSON'
  }).done(function (resp) {

    if(resp){
      $("#vl_documento").val((valorDocumento - valor).toFixed(2));
      $("#volume").val(volume - quantidade);
      $("#itens").val(($("#itens").val() - 1 ));

      let tr = $(handler).closest('tr');
      tr.remove();      

    }

  });

  return false;
};

function calcularTotalItem() {
  
  let quantidade = parseInt($('#quantidade_mov').val());
  let dolar = parseFloat($('#dolar').val()).toFixed(2);
  let custo = parseFloat($('#custo').val()).toFixed(2);

  let venda = parseFloat($('#venda').val()).toFixed(2);
  
  let totalItem = parseFloat((quantidade * custo)).toFixed(2);

  if (isNaN(totalItem)) totalItem = '0.00';

  $('#tot_item').val(totalItem);
}

$('#custo').blur(function() {
  let custo = parseFloat($(this).val());
  let margem = parseFloat($('#margem').val());
  let minimo =  parseFloat($('#minimo').val());
  let porcentagem = parseFloat($('#porcentagem').val());

  custo = (custo + ((custo/100) * porcentagem));
  minimo = (custo + ((custo/100) * margem));

  if (isNaN(custo)) custo = 0;
  if (isNaN(minimo)) minimo = 0;

  $(this).val(parseFloat(custo).toFixed(2));
  $('#minimo').val(parseFloat(minimo).toFixed(2));
  $('#venda').val(parseFloat(minimo).toFixed(2));
  calcularTotalItem();
  // alert(minimo)
})

$('#dolar').blur(function() {
  
  let quantidadeDolar = parseFloat($(this).val());
  let cotacao = parseFloat($('#dolar_cotacao').val());
  let margem = parseFloat($('#margem').val());

  alert(quantidadeDolar +' || '+ cotacao + ' || '+ margem);

  let custo = (quantidadeDolar * cotacao);

  if (isNaN(custo)) custo = 0;

  let minimo = (custo + ((custo/100) * margem));

  $('#custo').val(custo.toFixed(2));
  $('#minimo').val(parseFloat(minimo).toFixed(2));
  $('#venda').val(parseFloat(minimo).toFixed(2));
  calcularTotalItem();
})




//PDV


$('#addCliente').click(function(event) {

    var cliente_nome    = $('#cliente_nome').val();
        cliente_email   = $('#cliente_email').val();
        cliente_numero  = $('#cliente_numero').val();
        tipo            = $('#tipo').val();
        cpfCnpj         = $('#cpfCnpj').val();

      $.ajax({
        method: "POST",
        url: base_url+"PDV/addCliente/",
        dataType: "html",
        data: {
             cliente_nome: cliente_nome,
             cliente_email: cliente_email,
             cliente_numero: cliente_numero,
             tipo: tipo,
             cpfCnpj: cpfCnpj}
        })
        .done(function( response ) {
          console.log(response);

          location.reload();
        // $('#cliente_desc').html(response);
        // $('#cliente_desc').removeAttr("disabled", "disabled" );    
    
    });
});

// $('#categorias').click(function(event) {

//       $.ajax({
//         method: "POST",
//         url: base_url+"PDV/categorias/",
//         dataType: "html"
//         })
//         .done(function( response ) {
//           console.log(response);
//         $('#categorias_desc').html(response);
//         $('#categorias_desc').removeAttr("disabled", "disabled" );    
    
//     });
// });

// $(document).on('click', '.category', function () {

//   var grupo = $(this).attr('id');

//       $.ajax({
//         method: "POST",
//         url: base_url+"PDV/categoria_prod/",
//         dataType: "html",
//         data:{grupo: grupo}
//         })
//         .done(function( response ) {
//           console.log(response);

//           // location.reload();
//         $('#categoria_prod').html(response);
//         $('#categoria_prod').removeAttr("disabled", "disabled" );    
    
//     });
// });

  $('#grupo_prod').change(function(){
      var grupo = $('#grupo_prod').val();
      // alert(grupo);
      $.ajax({
        method: "POST",
        url: base_url+"movimentacao/selecionarProduto/",
        dataType: "html",
        data: { grupo: grupo}
      })
      .done(function( response ) {
        // console.log(response);
        $('#produto_desc').html(response);
        $('#produto_desc').removeAttr("disabled", "disabled" );   

        $('#produto_desc_mov').html(response); // View movimentação
        $('#produto_desc_mov').removeAttr("disabled", "disabled" ); // View movimentação     

      });

    });


//Autocompletes
$("#produto_descricao").autocomplete({

    // source: function(event, ui){
    //     var grupo = $('#grupo_prod').val();
    //     var term = $('#produto_descricao').val();
    //     $.get(base_url+"movimentacao/autoCompleteProduto/", {term:term, grupo:grupo})
    //     }, 
    source: base_url+"movimentacao/autoCompleteProduto",
    minLength: 2,
    select: function( event, ui ) {
      console.log('iasdiasduashd');
      $("#produto_id").val(ui.item.id);
      $("#estoque").val(ui.item.produto_estoque);         
      $("#quantidade").focus();   
    }
  });

$("#funcionario_nome_complete").autocomplete({
  source: base_url+"funcionarios/autoCompleteFuncionarios",
  minLength: 2,
  select: function( event, ui ) {
    $("#funcionario_id").val(ui.item.id);
  }
});

$("#funcionario_nome_auxilizar").autocomplete({
  source: base_url+"funcionarios/autoCompleteFuncionarios",
  minLength: 2,
  select: function( event, ui ) {       
  }
});

$("#cliente_nome").autocomplete({
  source: base_url+"clientes/autoCompleteClientes",
  minLength: 2,
  select: function( event, ui ) {    
    $("#cliente_id").val(ui.item.id);   
  }
});

$("#veiculo_placa").autocomplete({
  source: base_url+"veiculos/autoCompleteVeiculos",
  minLength: 2,
  select: function( event, ui ) {    
    $("#veiculo_id").val(ui.item.id);   
  }
});



//Mascaras
$('#placa').inputmask('aaa-9999');
$('#data').inputmask('99/99/9999');
$('.data').inputmask('99/99/9999');
$('#telefone').inputmask('(99)99999-9999');
$('#celular').inputmask('(99)99999-9999');
$('#cep').inputmask('99999-999');
$('#data_nascimento').inputmask('99/99/9999');
$('#cpf').inputmask('999.999.999-99');
$('#cnpj').inputmask('99.999.999/9999-99');
$('#hora').inputmask('99:99');
$('.hora').inputmask('99:99');




// $.ajax({
// 	url: base_url+'viagens/viagensEmAberto',
// 	dataType: 'json',
// }).always(function(response) {
// 	$('#viagensEmAberto').text(response.total);
// });



// $.ajax({
//     url: base_url+'sistema/notificacoes/',
//     dataType: 'json',
// })
// .done(function(response) {

//     var s1 = parseInt(response.api);
//     // var s1 = parseInt(response.ta);
//     //var s2 = parseInt(response.tce);
        
//      var s3 = s1 ;

//     $("#somaNotificacao").text(s3);
//     $("#apiNotificacao").text(response.api);
//    // $("#tceNotificacao").text(response.tce);
//    // $("#taNotificacao").text(response.ta);
// });



$('#alterarSenha').click(function(event) {

  $('.msg-error').text('');
  $('.msg-success').text('');

  var senha        = $('#senha_atual').val();
  var novaSenha    = $('#nova_senha').val();
  var confimaSenha = $('#confirma_senha').val();

  if(novaSenha != confimaSenha){
    $('.msg-error').text('A nova senha não são iguais!');
    return false;
  }

  $.ajax({
    url: base_url+'sistema/alterarSenha',
    method: 'POST',
    dataType: 'json',
    data: {senha: senha, novaSenha: novaSenha}
  }).always(function(response) {    

    if(response.status == true){
      $('.msg-success').text(response.msg);
    }else{
      $('.msg-error').text(response.msg);      
    }
  });
});

$('#adicionarMulta').click(function(event) {
  $('#adicionarMultas').css('display', 'block');
});




// ================= Rotina de Compra para Comercialização =================

var valor_total_compra = 0;

function ajustarTotal(i){
  
  let id = $(i).attr('id');
  let quantidadeNova = parseInt($(i).val());
  let quantidadeAnterior = parseInt($(i).attr('qtdAnterior'));
  let custo = $('#custo_'+id).text();

  let idCompra = parseInt($('#idCompra').val());


  if (isNaN(quantidadeNova)) quantidadeNova = 0;

  // Calcula a coluna 'Total'
  let valorTotalProduto = quantidadeNova * custo;
  if (isNaN(valorTotalProduto)) valorTotalProduto = 0;
  $('#total_'+id).text(valorTotalProduto < 0 ? '00.00' : valorTotalProduto.toFixed(2));

  // Atualiza a quantidade anterior
  $(i).attr('qtdAnterior', quantidadeNova);

  // Incrementa ou decrementa o valor total da compra.
  let resultValorProduto = (quantidadeNova - quantidadeAnterior) * custo;

  atualizarQuantidadeProduto(quantidadeNova, id, idCompra);
  somarValorTotalCompra(resultValorProduto);
}

// Função para atualizar o valor total da compra
function somarValorTotalCompra(valor) {
  valor_total_compra += valor;
  
  if(valor_total_compra <= 0){ 
    $("#btnPrepararCompra").hide();
  } else {
    $("#btnPrepararCompra").show();
  }

  $('#qtdtotal').text(valor_total_compra.toFixed(2));
  // $('#qtdtotal').text(valor_total_compra.toFixed(2));
}

function atualizarQuantidadeProduto(quantidade, id, idCompra) {
  $.ajax({
    url: base_url+'compra/atualizaQuantidadeProdutos/'+idCompra,
    type: 'POST',
    data: { quantProduto: quantidade, idProduto: id}
  }).done(function (resp) {
    console.log(resp);
  })
}
//===========================================================================


// ================ Rotina Preço Ajusta =====================================

function ajustarPreço(input){
  let id = $(input).attr('id');
  calculoGilberto(id);
  let valor = parseFloat($(input).val());
  let idProduto = $(input).attr('id');
  let idAjuste = $(input).attr('idAjuste');
  var credito = $(`[name=campoCC${id}]`).val();
  var debito = $(`[name=campoCD${id}]`).val();

  if (isNaN(valor)){
    valor = 0;
    $(input).val('0.00');
  } 

  $.ajax({
    url: base_url + "precoajuste/ajustarPrecoExe",
    type: "POST",
    data: { valor: valor, idProduto: idProduto, idAjuste: idAjuste, credito: credito, debito: debito}
  }).done(function (resp) {
    // console.log(resp);
  })
}

function ajustarPreçoCD(input){
  let valor = parseFloat($(input).val());
  let idProduto = $(input).attr('id');
  let idAjuste = $(input).attr('idAjuste');

  if (isNaN(valor)){
    valor = 0;
    $(input).val('0.00');
  } 

  $.ajax({
    url: base_url + "precoajuste/ajustarPrecoCDExe",
    type: "POST",
    data: { valor: valor, idProduto: idProduto, idAjuste: idAjuste}
  }).done(function (resp) {
    // console.log(resp);
  })
}

function ajustarPreçoCC(input){
  let valor = parseFloat($(input).val());
  let idProduto = $(input).attr('id');
  let idAjuste = $(input).attr('idAjuste');

  if (isNaN(valor)){
    valor = 0;
    $(input).val('0.00');
  } 

  $.ajax({
    url: base_url + "precoajuste/ajustarPrecoCCExe",
    type: "POST",
    data: { valor: valor, idProduto: idProduto, idAjuste: idAjuste}
  }).done(function (resp) {
    // console.log(resp);
  })
}

function calculoGilberto(id){

     let debito = $('#ajusteDebito').val();
     let credito = $('#ajusteCredto').val();
     let custo  = parseFloat($(`[name=campo${id}]`).val()).toFixed(2);

     let cal_debito   = parseFloat((custo/100)) * parseFloat(debito);
     let valor_debito = parseFloat(custo) + parseFloat(cal_debito);

     let cal_credito   = parseFloat((custo/100)) * parseFloat(credito);
     let valor_credito = parseFloat(custo) + parseFloat(cal_credito);

     $(`[name=campoCD${id}]`).val(valor_debito.toFixed(2));
     $(`[name=campoCC${id}]`).val(valor_credito.toFixed(2));
}

  $(`[name=campo]`).blur(function(){
    let id = $(this).attr('id');
    calculoGilberto(id);      
  });
// ================ Fim Rotina Preço Ajusta =================================

// ================ Rotina Estoque Ajusta =====================================

// $('tbody .precoAjuste').blur(function(){
//   console.log('aqui');
// });

function ajustarEstoque(input){
  let valor = $(input).val();
  let idProduto = $(input).attr('id');
  let idAjuste = $(input).attr('idAjuste');

  if(valor != ''){

      $.ajax({
        url: base_url + "estoque/ajustarEstoqueExe",
        type: "POST",
        data: { valor: valor, idProduto: idProduto, idAjuste: idAjuste}
      }).done(function (resp) {
        console.log(resp);
      })

  }
  

}

// ================ Fim Rotina Estoque Ajusta =================================


// ================ Função só para permitir Numeros =========================

function so_numero(e,args){             
  if (document.all){
    var evt=event.keyCode;
  } else {
    var evt = e.charCode;
  }   // do contrário deve ser Mozilla

  var valid_chars = '0123456789';      // criando a lista de teclas permitidas

  var chr= String.fromCharCode(evt);      // pegando a tecla digitada

  if (valid_chars.indexOf(chr)>-1 ){
    return true;
  } // se a tecla estiver na lista de permissão permite-a

  // para permitir teclas como <BACKSPACE> adicionamos uma permissão para 
  // códigos de tecla menores que 09 por exemplo (geralmente uso menores que 20)
  if (valid_chars.indexOf(chr)>-1 || evt < 9){ return true; } 

  if (valid_chars.indexOf(chr)>30 || evt <35){ return true; } //permite a tecla espaço

  return false;   // do contrário nega

}

// ================ Fim Função só para permitir Numeros =====================

// ================ Rotina Preço Cotacao fornecedor =====================================

function ajustarcotacaoPreco(input){
  let valor = parseFloat($(input).val());
  let idProduto = $(input).attr('id');
  let idAjuste  = $(input).attr('idAjuste');

  if (isNaN(valor)){
    valor = 0;
    $(input).val('0.00');
  } 

  $.ajax({
    url: base_url + "cotacaofornecedor/ajustarPrecoExe",
    type: "POST",
    data: { valor: valor, idProduto: idProduto, idAjuste: idAjuste}
  }).done(function (resp) {
    // console.log(resp);
  })
}

// ================ Fim Rotina Preço Cotacao fornecedor =================================

